#ifndef DELAYEDFUNCTION_HPP_INCLUDED
#define DELAYEDFUNCTION_HPP_INCLUDED

//STANDARD LIBRARY INCLUDES
#include <functional> // std::bind, std::function
#include <queue>      // std::queue
#include <utility>    // std::forward

namespace ui {

////////////////////////////////////////////////////////////////////////////////
/// \brief Used to call requested DelayedEvents.
///
/// When a DelayedEvent is called via its operator(), it pushes a function onto
/// a queue held within DelayedEventQueue. When run() is called,
/// DelayedEventQueue goes through this queue of functions, calling them all.
/// This allows for operations to be scheduled to be done at a later, safe,
/// time.
////////////////////////////////////////////////////////////////////////////////
class DelayedEventQueue {

private:

	std::queue<std::function<void ( void ) >> functions;

public:

	////////////////////////////////////////////////////////////////////////////
	/// \brief Add a function to be run later.
	///
	/// This function should only really be called by DelayedEvent, although it
	/// can be called manually. It adds the given function to a queue, which is
	/// then iterated through later. In cases of DelayedEvents with parameters
	/// that aren't void, passed values are bound to the DelayedEvent's
	/// function, and the result is given to the DelayedEventQueue.
	///
	/// \param function The function to be pushed onto the queue of functions.
	////////////////////////////////////////////////////////////////////////////
	void pushFunction (
		std::function<void ( void ) > function
	)
	{
		functions.push ( function );
	}

	////////////////////////////////////////////////////////////////////////////
	/// \brief Runs all held functions.
	///
	/// This function runs the function at the front of the contained queue of
	/// functions, then pops it from said queue. This means that functions that
	/// have been added to the queue are run in the order that they were added,
	/// i.e. the order in which the DelayedEvents were run.
	////////////////////////////////////////////////////////////////////////////
	void run()
	{
		while ( !functions.empty() ) {
			functions.front() ();
			functions.pop();
		}
	}

	////////////////////////////////////////////////////////////////////////////
	/// \brief Discards all held functions.
	///
	/// This gets rid of all held functions without running them. Use with care.
	////////////////////////////////////////////////////////////////////////////
	void clear()
	{
		while ( !functions.empty() ) {
			functions.pop();
		}
	}

};

////////////////////////////////////////////////////////////////////////////////
/// \brief A class used to schedule functions to be run later.
///
/// DelayedEvents works in conjunction with DelayedEventQueue. The DelayedEvent
/// acts like the function is contains, in that it can be called via its
/// operator() with the appropriate arguments. However this does not actually
/// run the contained function, but passes it on the the DelayedEventQueue,
/// which runs the function when run() is called.
////////////////////////////////////////////////////////////////////////////////
template <typename... T>
class DelayedEvent;

template <>
class DelayedEvent<void> {

private:

	DelayedEventQueue& queue;
	std::function<void ( void ) > task;

public:

	DelayedEvent (
		DelayedEventQueue&            delayedEventQueue,
		std::function<void ( void ) > task
	) :
		queue ( delayedEventQueue ),
		task ( task )
	{
	}

	void operator() ()
	{
		queue.pushFunction ( task );
	}

};

template <typename... T>
class DelayedEvent {

private:

	DelayedEventQueue& queue;
	std::function<void ( T... ) > task;

public:

	////////////////////////////////////////////////////////////////////////////
	/// \brief Constructor.
	///
	/// This links the DelayedEvent and DelayedEventQueue, and provides the
	/// function that will be scheduled and called. Because the DelayedEvent
	/// contains a reference the a DelayedEventQueue, not the other way around,
	/// the DelayedEventQueue should outlast the DelayedEvent, and it is not
	/// possible to link a DelayedEvent to a different DelayedEventQueue.
	///
	/// \param delayedEventQueue The DelayedEventQueue that can run this
	/// DelayedEvent's function.
	/// \param tasj The function to be scheduled/run.
	////////////////////////////////////////////////////////////////////////////
	DelayedEvent (
		DelayedEventQueue&            delayedEventQueue,
		std::function<void ( T... ) > task
	) :
		queue ( delayedEventQueue ),
		task ( task )
	{
	}

	////////////////////////////////////////////////////////////////////////////
	/// \brief Schedule the held function to be run.
	///
	/// This creates a callable object that doesn't take any parameters by using
	/// bind if parameters are passed, then pushes it to the DelayedEventQueue.
	///
	/// \param u The arguments to be bound to the function.
	////////////////////////////////////////////////////////////////////////////
	template <typename... U>
	void operator() (
		U&& ... u
	)
	{
		queue.pushFunction ( std::bind ( task, std::forward<U> ( u )... ) );
	}

};

} //END NAMESPACE ui

#endif // DELAYEDFUNCTION_HPP_INCLUDED
