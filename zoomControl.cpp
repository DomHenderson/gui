//STANDARD LIBRARY INCLUDES
#include <algorithm>        // std::find, std::remove_if
#include <initializer_list> // std::initializer_list
#include <string>           // std::stof, std::string, std::to_string
#include <queue>            // std::queue
#include <vector>           // std::vector

//SFGUI INCLUDES
#include <SFGUI/Adjustment.hpp> // sfg::Adjustment
#include <SFGUI/Box.hpp>        // sfg::Box
#include <SFGUI/Button.hpp>     // sfg::Button
#include <SFGUI/Desktop.hpp>    // sfg::Desktop
#include <SFGUI/Entry.hpp>      // sfg::Entry
#include <SFGUI/Frame.hpp>      // sfg::Frame
#include <SFGUI/Label.hpp>      // sfg::Label
#include <SFGUI/Scale.hpp>      // sfg::Scale
#include <SFGUI/Separator.hpp>  // sfg::Separator
#include <SFGUI/Widget.hpp>     // sfg::Widget
#include <SFGUI/Window.hpp>     // sfg::Window

//INTERNAL INCLUDES
#include "validator.hpp"   // ui::Validator::Trait, ui::Validator::ValidateEntry
#include "zoomControl.hpp" // ui::ZoomControl

namespace ui {

ZoomControl::ZoomControl (
	std::function<void(sf::Vector2f, sf::Vector2f, sf::Vector2f)> onZoomChange,
	DelayedEventQueue& delayedEventQueue
) :
	m_OnZoomChange ( delayedEventQueue, onZoomChange ),
	m_XZoomEntry ( sfg::Entry::Create ( "100" ) ),
	m_YZoomEntry ( sfg::Entry::Create ( "100" ) ),
	m_XPosEntry ( sfg::Entry::Create ( "0.00" ) ),
	m_YPosEntry ( sfg::Entry::Create ( "0.00" ) ),
	m_XSpacingEntry ( sfg::Entry::Create ( "1.00" ) ),
	m_YSpacingEntry ( sfg::Entry::Create ( "1.00" ) ),
	m_SetButton ( sfg::Button::Create ( "Set" ) ),
	m_Save { "100", "100", "0.00", "0.00", "1.00", "1.00" } //This contains the text from each widget when the settings were last set.
{
	//First set up the outer most containment widgets
	m_Frame = sfg::Frame::Create ( );
	auto outerBox = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
	auto innerBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );

	//Add the title
	outerBox->Pack ( sfg::Label::Create ( "Camera" ), false, false );
	outerBox->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );

	//Make the zoom section
	{
		//These boxes are used to line up all the widgets
		auto titleBox = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
		auto midBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
		auto leftBox = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
		auto rightBox = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );

		//Fill the left box with widgets to control the horizontal zoom
		leftBox->Pack ( sfg::Label::Create ( "X" ), false, false );
		m_XZoomEntry->SetRequisition ( { 50.f, 0.f } );
		leftBox->Pack ( m_XZoomEntry, true, true );
		Validator::ValidateEntry ( m_XZoomEntry, { Validator::Trait::Integer, Validator::Trait::Positive, Validator::Trait::NonZero } );

		//Fill the right box with widgets to control the vertical zoom
		rightBox->Pack ( sfg::Label::Create ( "Y" ), false, false );
		m_YZoomEntry->SetRequisition ( { 50.f, 0.f } );
		rightBox->Pack ( m_YZoomEntry, true, true );
		Validator::ValidateEntry ( m_YZoomEntry, { Validator::Trait::Integer, Validator::Trait::Positive, Validator::Trait::NonZero } );

		//Fill the middle box with the left and right boxes so that they are aligned next to each other
		midBox->Pack ( leftBox, true, true );
		midBox->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::VERTICAL ) );
		midBox->Pack ( rightBox, true, true );

		//Add a title
		titleBox->Pack ( sfg::Label::Create ( "Pixels per unit" ), false, false );
		titleBox->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );
		titleBox->Pack ( midBox, true, true );

		//Add everything to the main widget
		innerBox->Pack ( titleBox, false, true );
	}

	//Make the position section
	{
		//Boxes for alignment
		auto titleBox = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
		auto midBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
		auto leftBox = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
		auto rightBox = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );

		//Fill the left box with widgets to control the horizontal position
		leftBox->Pack ( sfg::Label::Create ( "X" ), false, false );
		m_XPosEntry->SetRequisition ( { 50.f, 0.f } );
		leftBox->Pack ( m_XPosEntry, true, true );
		Validator::ValidateEntry ( m_XPosEntry, Validator::Trait::Decimal );

		//Fill the right box with widgets to control the vertical position
		rightBox->Pack ( sfg::Label::Create ( "Y" ), false, false );
		m_YPosEntry->SetRequisition ( { 50.f, 0.f } );
		rightBox->Pack ( m_YPosEntry, true, true );
		Validator::ValidateEntry ( m_YPosEntry, Validator::Trait::Decimal );

		//Fill the middle box with the left and right boxes so that they are aligned next to each other
		midBox->Pack ( leftBox, true, true );
		midBox->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::VERTICAL ) );
		midBox->Pack ( rightBox, true, true );

		//Add a title
		titleBox->Pack ( sfg::Label::Create ( "Centre" ), false, false );
		titleBox->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );
		titleBox->Pack ( midBox, true, true );

		//Add everything to the main widget
		innerBox->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::VERTICAL ), false, true );
		innerBox->Pack ( titleBox, false, true );
	}

	//Make the ticks section
	{
		//Boxes for alignment
		auto titleBox = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
		auto midBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
		auto leftBox = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
		auto rightBox = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );

		//Fill thhe left box with widgets to control the horizontal tick spacing
		leftBox->Pack ( sfg::Label::Create ( "X" ), false, false );
		m_XSpacingEntry->SetRequisition ( { 50.f, 0.f } );
		leftBox->Pack ( m_XSpacingEntry, true, true );
		Validator::ValidateEntry ( m_XSpacingEntry, { Validator::Trait::Decimal, Validator::Trait::Positive, Validator::Trait::NonZero } );

		//Fill the right box with widgets to control the vertical tick spacing
		rightBox->Pack ( sfg::Label::Create ( "Y" ), false, false );
		m_YSpacingEntry->SetRequisition ( { 50.f, 0.f } );
		rightBox->Pack ( m_YSpacingEntry, true, true );
		Validator::ValidateEntry ( m_YSpacingEntry, { Validator::Trait::Decimal, Validator::Trait::Positive, Validator::Trait::NonZero } );

		//Fill the middle box with the left and right boxes so that they are aligned next to each other
		midBox->Pack ( leftBox, true, true );
		midBox->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::VERTICAL ) );
		midBox->Pack ( rightBox, true, true );

		//Add a title
		titleBox->Pack ( sfg::Label::Create ( "Tick spacing" ),false, false );
		titleBox->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );
		titleBox->Pack ( midBox, true, true );

		//Add everything to the main widget
		innerBox->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::VERTICAL ), false, true );
		innerBox->Pack ( titleBox, false, true );
	}

	outerBox->Pack ( innerBox, false, true );
	outerBox->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );
	outerBox->Pack ( m_SetButton, false, true );

	//Because SFGUI provides no signal for enter being pressed when an Entry is selected, a set button is needed to apply the changes.
	m_SetButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] () {
		//The camera should only be changed if all the text entered is valid.
		//There is no need to further inform the user if it is invalid, the red text has proven very effective.
		if ( m_XZoomEntry->GetId() != "Invalid" && m_YZoomEntry->GetId() != "Invalid" &&
		     m_XPosEntry->GetId() != "Invalid" && m_YPosEntry->GetId() != "Invalid" &&
		     m_XSpacingEntry->GetId() != "Invalid" && m_YSpacingEntry->GetId() != "Invalid"
		) {
			m_OnZoomChange (
				sf::Vector2f (
					std::stof ( std::string ( m_XZoomEntry->GetText() ) ), //std::stof cannot convert directly from an sf::String,
					std::stof ( std::string ( m_YZoomEntry->GetText() ) ) //so an explicit conversion to std::string is required.
				),
				sf::Vector2f (
					std::stof ( std::string ( m_XPosEntry->GetText() ) ),
					std::stof ( std::string ( m_YPosEntry->GetText() ) )
				),
				sf::Vector2f (
					std::stof ( std::string ( m_XSpacingEntry->GetText() ) ),
					std::stof ( std::string ( m_YSpacingEntry->GetText() ) )
				)
			);
			m_Save.xZoom = m_XZoomEntry->GetText();
			m_Save.yZoom = m_YZoomEntry->GetText();
			m_Save.xPos = m_XPosEntry->GetText();
			m_Save.yPos = m_YPosEntry->GetText();
			m_Save.xSpacing = m_XSpacingEntry->GetText();
			m_Save.ySpacing = m_YSpacingEntry->GetText();
		}
	} );

	m_Frame->Add ( outerBox );
}

sfg::Widget::Ptr ZoomControl::getWidget() const
{
	return m_Frame;
}

void ZoomControl::reset()
{
	m_XZoomEntry->SetText ( "100" );
	m_YZoomEntry->SetText ( "100" );
	m_XPosEntry->SetText ( "0.00" );
	m_YPosEntry->SetText ( "0.00" );
	m_XSpacingEntry->SetText ( "1.00" );
	m_YSpacingEntry->SetText ( "1.00" );

	//Simulating a button press ensures that the settings in the gui are reset as well.
	m_SetButton->GetSignal ( sfg::Button::OnLeftClick )();
}

void ZoomControl::save ( std::queue<std::string>& saveInformation )
{
	//All the required information is already in the save struct, so it simply needs to be pushed in the same order that it'll be read.
	saveInformation.push ( m_Save.xZoom );
	saveInformation.push ( m_Save.yZoom );
	saveInformation.push ( m_Save.xPos );
	saveInformation.push ( m_Save.yPos );
	saveInformation.push ( m_Save.xSpacing );
	saveInformation.push ( m_Save.ySpacing );
}

void ZoomControl::load ( std::queue<std::string>& saveInformation )
{
	//Read and remove the relevant information from saveInformation.
	//This leaves it ready to be read by the next class that it will be passed to.
	m_Save.xZoom = saveInformation.front();
	saveInformation.pop();
	m_Save.yZoom = saveInformation.front();
	saveInformation.pop();
	m_Save.xPos = saveInformation.front();
	saveInformation.pop();
	m_Save.yPos = saveInformation.front();
	saveInformation.pop();
	m_Save.xSpacing = saveInformation.front();
	saveInformation.pop();
	m_Save.ySpacing = saveInformation.front();
	saveInformation.pop();

	//Sync the gui with the loaded data.
	m_XZoomEntry->SetText ( m_Save.xZoom );
	m_YZoomEntry->SetText ( m_Save.yZoom );
	m_XPosEntry->SetText ( m_Save.xPos );
	m_YPosEntry->SetText ( m_Save.yPos );
	m_XSpacingEntry->SetText ( m_Save.xSpacing );
	m_YSpacingEntry->SetText ( m_Save.ySpacing );

	//Make sure that the loaded data is valid.
	m_XZoomEntry->GetSignal ( sfg::Entry::OnTextChanged )();
	m_YZoomEntry->GetSignal ( sfg::Entry::OnTextChanged )();
	m_XPosEntry->GetSignal ( sfg::Entry::OnTextChanged )();
	m_YPosEntry->GetSignal ( sfg::Entry::OnTextChanged )();
	m_XSpacingEntry->GetSignal ( sfg::Entry::OnTextChanged )();
	m_YSpacingEntry->GetSignal ( sfg::Entry::OnTextChanged )();

	//If all the data was valid, apply it.
	m_SetButton->GetSignal ( sfg::Button::OnLeftClick )();
}

} //END NAMESPACE ui
