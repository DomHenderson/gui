//STANDARD LIBRARY INCLUDES
#include <queue>   //std::queue
#include <string>  //std::string, std::to_string, std::stod, std::stoul
#include <sstream> //std::stringstream

//SFGUI INCLUDES
#include <SFGUI/Box.hpp>            //sfg::Box
#include <SFGUI/Button.hpp>         //sfg::Button
#include <SFGUI/Desktop.hpp>        //sfg::Desktop
#include <SFGUI/Label.hpp>          //sfg::Label
#include <SFGUI/Notebook.hpp>       //sfg::Notebook
#include <SFGUI/ScrolledWindow.hpp> //sfg::ScrolledWindow
#include <SFGUI/Separator.hpp>      //sfg::Separator

//EXPRESSION COMPILER INCLUDES
#include <ExpressionCompiler2/compiler.hpp> //ec::Compiler

//INTERNAL INCLUDES
#include "delayedFunction.hpp" //ui::DelayedEvent
#include "libraryWidget.hpp"   //ui::LibraryWidget
#include "validator.hpp"       //ui::Validator

namespace ui {

//Converts a name and associated value into a class stored by the LibraryWidget, and adds information to the compiler.
LibraryWidget::Constant LibraryWidget::CreateConstant ( std::string name, double value )
{
	Constant constant;
	constant.value = value;
	constant.widget = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	constant.widget->Pack ( sfg::Label::Create ( name + " = " + std::to_string ( value ) ), false, false );
	auto closeButton = sfg::Button::Create ( "X" );
	closeButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ name, this ] () {
		RemoveConstant ( name );
	} );
	constant.widget->Pack ( closeButton, false, false );

	m_Compiler.addConstant ( name, value );

	return constant;
}

//Produces a graphical entry in the LibraryWidget, and registers the function with the compiler.
LibraryWidget::Function LibraryWidget::CreateFunction ( std::string name, std::string expression, std::string input )
{
	Function function;
	function.name = name;
	function.expression = expression;
	function.input = input;
	function.widget = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	function.widget->Pack ( sfg::Label::Create ( name + " : " + input + " -> " + expression ), false, false );
	auto closeButton = sfg::Button::Create ( "X" );
	closeButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ name, this ] () {
		RemoveFunction ( name );
	} );
	function.widget->Pack ( closeButton, false, false );

	m_Compiler.addFunction ( name, input, expression );

	return function;
}

LibraryWidget::LibraryWidget ( DelayedEventQueue& delayedEventQueue, sfg::Desktop& desktop, ec::Compiler& compiler ) :
	m_Compiler ( compiler ),
	RemoveConstant ( delayedEventQueue, [ this ] ( std::string name ) { //DelayedEvents are used for this because altering SFGUI's
		m_ConstantsBox->Remove ( m_Constants [ name ].widget );         //during the updating function can cause crashes.
		m_Constants.erase ( name );
		m_Compiler.removeConstant ( name );
	} ),
	RemoveFunction ( delayedEventQueue, [ this ] ( std::string name ) {
		m_FunctionsBox->Remove ( m_Functions [ name ].widget );
		m_Functions.erase ( name );
		m_Compiler.removeFunction ( name );
	})
{
	m_MainBox = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
	m_MainBox->Pack ( sfg::Label::Create ( "Library" ), false, false );

	auto notebook = sfg::Notebook::Create (); //This allows functions and constants to be displayed separately without taking up lots of space.

	m_ConstantsBox = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
	m_FunctionsBox = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );

	//The pages start of simply as empty ScrolledWindows, as no constants or functions have been added  yet.

	//Page 1
	{
		auto box = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
		auto newButton = sfg::Button::Create ( "New function" );
		newButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this, &desktop ] () {
			m_AddFunctionWindow->Show ( true );
			desktop.BringToFront ( m_AddFunctionWindow );
		} );
		auto listWindow = sfg::ScrolledWindow::Create ();
		listWindow->SetScrollbarPolicy (
			sfg::ScrolledWindow::ScrollbarPolicy::HORIZONTAL_AUTOMATIC |
			sfg::ScrolledWindow::ScrollbarPolicy::VERTICAL_AUTOMATIC
		);
		listWindow->AddWithViewport ( m_FunctionsBox );
		box->Pack ( newButton, false, true );
		box->Pack ( listWindow, true, true );

		auto pageSwitchButton = sfg::Button::Create ( "Functions" );
		pageSwitchButton->GetSignal ( sfg::Button::OnLeftClick).Connect ( [ notebook ] () {
			notebook->PreviousPage();
		} );
		notebook->AppendPage ( box, pageSwitchButton );
	}

	//Page 2
	{
		auto box = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
		auto newButton = sfg::Button::Create ( "New constant" );
		newButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this, &desktop ] () {
			m_AddConstantWindow->Show ( true );
			desktop.BringToFront ( m_AddConstantWindow );
		} );

		auto listWindow = sfg::ScrolledWindow::Create ();
		listWindow->SetScrollbarPolicy (
			sfg::ScrolledWindow::ScrollbarPolicy::HORIZONTAL_AUTOMATIC |
			sfg::ScrolledWindow::ScrollbarPolicy::VERTICAL_AUTOMATIC
		);
		listWindow->AddWithViewport ( m_ConstantsBox );
		box->Pack ( newButton, false, true );
		box->Pack ( listWindow, true, true );

		auto pageSwitchButton = sfg::Button::Create ( "Constants" );
		pageSwitchButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ notebook ] () {
			notebook->NextPage();
		} );
		notebook->AppendPage ( box, pageSwitchButton );
	}

	m_MainBox->Pack ( notebook, true, true );

	//Add the standard constants which should be recognised automatically, unless the user removes them.
	addConstant ( "e", 2.718281828 );
	addConstant ( "pi", 3.14159265359 );
	addConstant ( "phi", 1.61803398875 );

	notebook->SetCurrentPage(1); //It make sense to display the constants page initially, as it's the only one with anything in it.

	//Add constant window - this handles getting all the information for a new constant from the user.
	{
		m_AddConstantWindow = sfg::Window::Create ();
		auto box = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
		box->Pack ( sfg::Label::Create ( "Register a new constant" ), false, false );
		box->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );

		auto entryBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
		auto preexistingNameLabel = sfg::Label::Create ( "A constant with that name already exists" );

		auto nameBox = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
		auto nameEntry = sfg::Entry::Create ();
		Validator::ValidateEntry ( nameEntry, Validator::Trait::Alphabetic );
		nameEntry->GetSignal ( sfg::Entry::OnTextChanged ).Connect ( [ this, nameEntry, preexistingNameLabel ] () {
			preexistingNameLabel->Show ( m_Constants.find ( nameEntry->GetText() ) != m_Constants.end() );
		} );
		nameBox->Pack ( sfg::Label::Create ( "Name" ), false, false );
		nameBox->Pack ( nameEntry, true, true );

		auto valueBox = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
		auto valueEntry = sfg::Entry::Create ();
		Validator::ValidateEntry ( valueEntry, Validator::Trait::Decimal );
		valueBox->Pack ( sfg::Label::Create ( "Value" ), false, false );
		valueBox->Pack ( valueEntry, true, true );

		entryBox->Pack ( nameBox, true, true );
		entryBox->Pack ( valueBox, true, true );

		box->Pack ( entryBox, false, true );
		box->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );
		box->Pack ( preexistingNameLabel, false, false );

		auto invalidNameLabel = sfg::Label::Create ( "Invalid name" );
		box->Pack ( invalidNameLabel, false, false );

		auto invalidValueLabel = sfg::Label::Create ( "Invalid value" );
		box->Pack ( invalidValueLabel, false, false );

		auto buttonBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );

		auto cancelButton = sfg::Button::Create ( "Cancel" );
		cancelButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this, nameEntry, valueEntry, preexistingNameLabel ] () {
			nameEntry->SetText ( "" );
			valueEntry->SetText ( "" );
			preexistingNameLabel->Show ( false );
			m_AddConstantWindow->Show ( false );
		} );
		buttonBox->Pack ( cancelButton, true, true );

		auto doneButton = sfg::Button::Create ( "Done" );
		doneButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this, nameEntry, valueEntry, preexistingNameLabel, invalidNameLabel, invalidValueLabel ] () {
			if ( m_Constants.find ( nameEntry->GetText() ) != m_Constants.end() ) {
				preexistingNameLabel->Show ( true );
				invalidNameLabel->Show ( false );
				invalidValueLabel->Show ( false );
			} else if ( nameEntry->GetId() == "Invalid" ) {
				preexistingNameLabel->Show ( false );
				invalidNameLabel->Show ( true );
				invalidValueLabel->Show ( false );
			} else if ( valueEntry->GetId() == "Invalid" ) {
				preexistingNameLabel->Show ( false );
				invalidNameLabel->Show ( false );
				invalidValueLabel->Show ( true );
			} else {
				addConstant ( nameEntry->GetText(), std::stod ( static_cast<std::string> ( valueEntry->GetText() ) ) );
				nameEntry->SetText ( "" );
				valueEntry->SetText ( "" );
				preexistingNameLabel->Show ( false );
				m_AddConstantWindow->Show ( false );
			}
		} );
		buttonBox->Pack ( doneButton, true, true );

		box->Pack ( buttonBox, false, true );

		m_AddConstantWindow->Add ( box );
		m_AddConstantWindow->Show ( false );

		preexistingNameLabel->Show ( false );
		invalidNameLabel->Show ( false );
		invalidValueLabel->Show ( false );

		desktop.Add( m_AddConstantWindow );
	}

	//Add function window - this gets all the information required for a new function.
	{
		m_AddFunctionWindow = sfg::Window::Create ();

		auto box = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
		box->Pack ( sfg::Label::Create ( "Register a new function" ), false, false );
		box->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ) );

		auto topBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );

		auto nameBox = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
		nameBox->Pack ( sfg::Label::Create ( "Name" ), false, false );
		auto nameEntry = sfg::Entry::Create ();
		Validator::ValidateEntry ( nameEntry, Validator::Trait::Alphabetic );
		nameBox->Pack ( nameEntry, true, true );

		auto inputBox = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
		inputBox->Pack ( sfg::Label::Create ( "Input" ), false, false );
		auto inputEntry = sfg::Entry::Create ();
		Validator::ValidateEntry ( inputEntry, Validator::Trait::Alphabetic );
		inputBox->Pack ( inputEntry, true, true );

		topBox->Pack ( nameBox, true, true );
		topBox->Pack ( inputBox, true, true );

		box->Pack ( topBox, false, true );
		box->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ) );
		box->Pack ( sfg::Label::Create ( "Expression" ) );
		auto expressionEntry = sfg::Entry::Create ();
		box->Pack ( expressionEntry, false, true );
		box->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ) );

		auto preExistingNameLabel = sfg::Label::Create ( "A function with that name already exists" );
		auto invalidNameLabel = sfg::Label::Create ( "Invalid name " );
		auto invalidInputLabel = sfg::Label::Create ( "Invalid input" );

		box->Pack ( preExistingNameLabel, false, false );
		box->Pack ( invalidNameLabel, false, false );
		box->Pack ( invalidInputLabel, false, false );

		preExistingNameLabel->Show ( false );
		invalidNameLabel->Show ( false );
		invalidInputLabel->Show ( false );

		nameEntry->GetSignal ( sfg::Entry::OnTextChanged ).Connect ( [ this, nameEntry, preExistingNameLabel ] () {
			preExistingNameLabel->Show ( m_Functions.find ( nameEntry->GetText() ) != m_Functions.end() );
		} );

		auto buttonBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
		auto cancelButton = sfg::Button::Create ( "Cancel" );
		cancelButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this, nameEntry, inputEntry, expressionEntry, preExistingNameLabel, invalidNameLabel, invalidInputLabel ] () {
			preExistingNameLabel->Show ( false );
			invalidNameLabel->Show ( false );
			invalidInputLabel->Show ( false );
			nameEntry->SetText ( "" );
			inputEntry->SetText ( "" );
			expressionEntry->SetText ( "" );
			m_AddFunctionWindow->Show ( false );
		} );

		auto doneButton = sfg::Button::Create ( "Done" );
		doneButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this, nameEntry, inputEntry, expressionEntry, preExistingNameLabel, invalidNameLabel, invalidInputLabel ] () {
			if ( m_Functions.find ( nameEntry->GetText() ) != m_Functions.end() ) {
				preExistingNameLabel->Show ( true );
				invalidNameLabel->Show ( false );
				invalidInputLabel->Show ( false );
			} else if ( nameEntry->GetId () == "Invalid" ) {
				preExistingNameLabel->Show ( false );
				invalidNameLabel->Show ( true );
				invalidInputLabel->Show ( false );
			} else if ( inputEntry->GetId () == "Invalid" ) {
				preExistingNameLabel->Show ( false );
				invalidNameLabel->Show ( false );
				invalidInputLabel->Show ( true );
			} else {
				preExistingNameLabel->Show ( false );
				invalidNameLabel->Show ( false );
				invalidInputLabel->Show ( false );
				addFunction ( nameEntry->GetText(), expressionEntry->GetText(), inputEntry->GetText() );
				nameEntry->SetText ( "" );
				inputEntry->SetText ( "" );
				expressionEntry->SetText ( "" );
				m_AddFunctionWindow->Show ( false );
			}
		} );

		buttonBox->Pack ( cancelButton, true, true );
		buttonBox->Pack ( doneButton, true, true );

		box->Pack ( buttonBox, false, true );

		m_AddFunctionWindow->Add ( box );
		desktop.Add ( m_AddFunctionWindow );
		m_AddFunctionWindow->Show ( false );
	}
}

void LibraryWidget::addConstant ( std::string name, double value )
{
	//Store the Constant itself.
	m_Constants[name] = CreateConstant ( name, value );

	//Display its entry in the notebook.
	m_ConstantsBox->Pack ( m_Constants[name].widget, false, false );
}

void LibraryWidget::addFunction ( std::string name, std::string expression, std::string input )
{
	//Store the function itself.
	m_Functions[name] = CreateFunction ( name, expression, input );

	//Display its entry in the notebook.
	m_FunctionsBox->Pack ( m_Functions[name].widget, false, false );
}

void LibraryWidget::reset ()
{
	//Remove all the existing entries and deregister them. Then add the default ones back.
	for ( auto& x: m_Constants ) {
		m_Compiler.removeConstant ( x.first );
	}
	m_Constants.clear();
	m_ConstantsBox->RemoveAll();
	addConstant ( "e", 2.718281828 );
	addConstant ( "pi", 3.14159265359 );
	addConstant ( "phi", 1.61803398875 );

	for ( auto& x: m_Functions ) {
		m_Compiler.removeFunction ( x.first );
	}
	m_Functions.clear();
	m_FunctionsBox->RemoveAll();
}

void LibraryWidget::save ( std::queue<std::string>& saveInformation )
{
	//First push all the information stored in all the constants into the queue.
	saveInformation.push ( std::to_string ( m_Constants.size() ) );
	for ( auto& x: m_Constants ) {
		saveInformation.push ( x.first );

		//A stringstream is used so that no precision is lost.
		std::string valueString;
		std::stringstream ss;
		ss << std::setprecision ( 15 ) << x.second.value;
		ss >> valueString;

		saveInformation.push ( valueString );
	}

	//Then push all of the information stored in the functions.
	saveInformation.push ( std::to_string ( m_Functions.size() ) );
	for ( auto& x: m_Functions ) {
		saveInformation.push ( x.first );
		saveInformation.push ( x.second.input );
		saveInformation.push ( x.second.expression );
	}
}

void LibraryWidget::load ( std::queue<std::string>& saveInformation )
{
	//Empty the library widget, so that it is ready to be reloaded.
	//This is done manually rather than with reset(), because reset puts the default constants in.
	for ( auto& x: m_Constants ) {
		m_Compiler.removeConstant ( x.first );
	}
	m_Constants.clear();
	m_ConstantsBox->RemoveAll();
	m_Functions.clear();
	m_FunctionsBox->RemoveAll();

	unsigned numOfConstants = std::stoul ( saveInformation.front() );
	saveInformation.pop();
	for ( unsigned i (0); i < numOfConstants; ++i ) {
		std::string name = saveInformation.front();
		saveInformation.pop();
		std::string value = saveInformation.front();
		saveInformation.pop();

		addConstant ( name, std::stod ( value ) );
	}
	unsigned numOfFunctions = std::stoul ( saveInformation.front() );
	saveInformation.pop();
	for ( unsigned i (0); i < numOfFunctions; ++i ) {
		std::string name = saveInformation.front();
		saveInformation.pop();
		std::string input = saveInformation.front();
		saveInformation.pop();
		std::string expression = saveInformation.front();
		saveInformation.pop();

		addFunction ( name, expression, input );
	}
}

sfg::Widget::Ptr LibraryWidget::getWidget() const
{
	return m_MainBox;
}

}
