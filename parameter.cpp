//STANDARD LIBRARY INCLUDES
#include <functional> // std::function
#include <string>     // std::stod, std::stof, std::stoul, std::string, std::to_string
#include <vector>     // std::vector

//SFGUI INCLULDES
#include <SFGUI/Adjustment.hpp>  // sfg::Adjustment
#include <SFGUI/Box.hpp>         // sfg::Box
#include <SFGUI/Button.hpp>      // sfg::Button
#include <SFGUI/ComboBox.hpp>    // sfg::ComboBox
#include <SFGUI/Entry.hpp>       // sfg::Entry
#include <SFGUI/Label.hpp>       // sfg::Label
#include <SFGUI/ProgressBar.hpp> // sfg::ProgressBar
#include <SFGUI/Scale.hpp>       // sfg::Scale
#include <SFGUI/Separator.hpp>   // sfg::Separator
#include <SFGUI/Widget.hpp>      // sfg::Widget

//INTERNAL INCLUDES
#include "delayedFunction.hpp" // ui::DelayedEvent, ui::DelayedEventQueue
#include "parameter.hpp"       // ui::ConstantParameter, ui::SliderParameter, ui::TimeParameter
#include "validator.hpp"       // ui::Validator::Trait, ui::Validator::ValidateEntry

namespace ui {

std::unique_ptr<Parameter> Parameter::load ( std::queue<std::string>& saveInformation, DelayedEventQueue& delayedEventQueue, std::vector<std::weak_ptr<std::function<void ( float ) >>>& updateQueue )
{
	//The different types of parameters all save information differently.
	//However they all save an identifying string first so that the correct load function can be chosen.
	std::string type = saveInformation.front();
	saveInformation.pop();
	if ( type == "Slider" ) {
		return SliderParameter::load ( saveInformation, delayedEventQueue );
	} else if ( type == "Constant" ) {
		return ConstantParameter::load ( saveInformation, delayedEventQueue );
	} else if ( type == "Time" ) {
		return TimeParameter::load ( saveInformation, delayedEventQueue, updateQueue );
	} else {
		return std::unique_ptr<Parameter>();
	}
}

SliderParameter::SliderParameter ( DelayedEventQueue& delayedEventQueue ) :
	m_Save { "0.0", "1.0", "20", 0.f },
	UpdateUserLabel ( delayedEventQueue, [ this ] ( ) {
		m_Label->SetText ( std::to_string ( m_Slider->GetValue ( ) ) );
	} ),
	UpdateUserSlider ( delayedEventQueue, [ this ] ( ) {
		m_Save.lower = m_LowerEntry->GetText(); //This needs to be done for saving
		m_Save.upper = m_UpperEntry->GetText(); //However doing things in this order gets rid of static casts
		m_Save.steps = m_StepsEntry->GetText();

		const float lower ( std::stof ( m_Save.lower ) ); //If the text from the entires was used directly,
		const float upper ( std::stof ( m_Save.upper ) ); //it would have to be static casted, which is harder to read.
		const float stepSize ( ( upper - lower ) / std::stoul ( m_Save.steps ) );
		m_Slider->SetRange ( lower, upper );
		m_Slider->SetIncrements ( stepSize, 0.f );
		m_Label->SetText ( std::to_string ( m_Slider->GetValue ( ) ) );
	} )
{
	//Create modification Widget
	//Extra scope is used to avoid having to preface all names like modificationWidgetTitle, or modifiationWidgetBox.
	{
		auto title = sfg::Label::Create ( "Slider Parameter" );
		auto topSeparator = sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL );
		auto box = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
		box->Pack ( title, false, false );
		box->Pack ( topSeparator, false, true );

		auto lowerLabel = sfg::Label::Create ( "Lower bound" );
		m_LowerEntry = sfg::Entry::Create ( "0.0" );
		auto lowerBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
		lowerBox->Pack ( lowerLabel, false, false );
		lowerBox->Pack ( m_LowerEntry, true, true );
		box->Pack ( lowerBox, false, true );

		auto upperLabel = sfg::Label::Create ( "Upper bound" );
		m_UpperEntry = sfg::Entry::Create ( "1.0" );
		auto upperBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
		upperBox->Pack ( upperLabel, false, false );
		upperBox->Pack ( m_UpperEntry, true, true );
		box->Pack ( upperBox, false, true );

		auto stepsLabel = sfg::Label::Create ( "Number of steps" );
		m_StepsEntry = sfg::Entry::Create ( "20" );
		auto stepsBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
		m_StepsEntry->SetRequisition ( {50.f, 0.f} );
		stepsBox->Pack ( stepsLabel, false, false );
		stepsBox->Pack ( m_StepsEntry, true, true );
		box->Pack ( stepsBox, false, true );

		auto bottomSeparator = sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL );
		auto setButton = sfg::Button::Create ( "Set" );
		box->Pack ( bottomSeparator, false, true );
		box->Pack ( setButton, false, false );

		m_ModificationWidget = box; //This makes it easy to modify if wanted

		//Signals
		setButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] ( ) {
			UpdateUserSlider();
		} );

		Validator::ValidateEntry ( m_LowerEntry, Validator::Trait::Decimal );
		Validator::ValidateEntry ( m_UpperEntry, Validator::Trait::Decimal );
		Validator::ValidateEntry ( m_StepsEntry, { Validator::Trait::Positive, Validator::Trait::Integer, Validator::Trait::NonZero } );
	}

	//Create user Widget
	{
		//Name of the parameter is stored outside of the parameter,
		//so displaying it doesn't need to be handled here

		m_Label = sfg::Label::Create ( "0.0" );
		m_Slider = sfg::Scale::Create ( 0, 1, 0.02, sfg::Scale::Orientation::HORIZONTAL );
		m_Slider->SetRequisition ( {100.f, 10.f} );
		auto box = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
		box->Pack ( m_Label, false, false );
		box->Pack ( m_Slider, false, true );

		m_Slider->GetAdjustment()->GetSignal ( sfg::Adjustment::OnChange ).Connect ( [this]  {
			UpdateUserLabel ( );
			m_Save.value = m_Slider->GetValue();
		} );

		m_UserWidget = box;
	}
}

double SliderParameter::getValue() const
{
	return m_Slider->GetValue();
}

sfg::Widget::Ptr SliderParameter::getModificationWidget() const
{
	return m_ModificationWidget;
}

sfg::Widget::Ptr SliderParameter::getUserWidget() const
{
	return m_UserWidget;
}

void SliderParameter::save ( std::queue<std::string>& saveInformation ) const
{
	saveInformation.push ( "Slider" );
	saveInformation.push ( m_Save.lower );
	saveInformation.push ( m_Save.upper );
	saveInformation.push ( m_Save.steps );
	saveInformation.push ( std::to_string ( m_Save.value ) );
}

std::unique_ptr<Parameter> SliderParameter::load ( std::queue<std::string>& saveInformation, DelayedEventQueue& delayedEventQueue )
{
	//The line containing "Slider" has already been removed by Parameter::load
	std::unique_ptr<SliderParameter> slider = std::make_unique<SliderParameter>( delayedEventQueue );
	slider->m_Save.lower = saveInformation.front();
	saveInformation.pop();
	slider->m_Save.upper = saveInformation.front();
	saveInformation.pop();
	slider->m_Save.steps = saveInformation.front();
	saveInformation.pop();
	slider->m_Save.value = std::stod ( saveInformation.front() );
	saveInformation.pop();

	slider->m_LowerEntry->SetText ( slider->m_Save.lower );
	slider->m_UpperEntry->SetText ( slider->m_Save.upper );
	slider->m_StepsEntry->SetText ( slider->m_Save.steps );
	slider->m_Slider->SetValue ( slider->m_Save.value );

	slider->UpdateUserLabel();
	slider->UpdateUserSlider();

	return slider;
}

ConstantParameter::ConstantParameter( DelayedEventQueue& delayedEventQueue ):
	m_Value ( 0.0 ),
	m_SavedText ( "0.0" ),
	UpdateValue ( delayedEventQueue, [ this ] () {
		m_SavedText = m_ValueEntry->GetText();
		m_Value = std::stod ( m_SavedText ); //This order removes the need for a cast.
	} )
{
	//ConstantParameter is different to SliderParameter and TimeParameter.
	//As it only stores one item of data, its value, this is modified directly in the user widget, and there is no modification widget.
	//Create user widget
	auto title = sfg::Label::Create ( "Constant parameter" );
	auto topSeparator = sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL );
	auto valueLabel = sfg::Label::Create ( "Value" );
	m_ValueEntry = sfg::Entry::Create();
	m_ValueEntry->SetText( "0.0" );
	auto bottomSeparator = sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL );
	auto setButton = sfg::Button::Create ( "Set" );

	auto box = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
	box->Pack ( title, false, false );
	box->Pack ( topSeparator, false, true );
	box->Pack ( valueLabel, false, false );
	box->Pack ( m_ValueEntry, false, true );
	box->Pack ( bottomSeparator, false, true );
	box->Pack ( setButton, false, true );

	m_UserWidget = box;

	Validator::ValidateEntry ( m_ValueEntry, Validator::Trait::Decimal );

	setButton->GetSignal ( sfg::Button::OnLeftClick ).Connect( [ this ] () {
		UpdateValue();
	} );

	//ConstantParameters do not have a modification widget, as they are simply a value, which is substituted in.
	m_ModificationWidget = sfg::Label::Create ( "N/A" ); //However something still has to be returned, or the program will crash.
}

double ConstantParameter::getValue() const
{
	return m_Value;
}

sfg::Widget::Ptr ConstantParameter::getModificationWidget() const
{
	return m_ModificationWidget;
}

sfg::Widget::Ptr ConstantParameter::getUserWidget() const
{
	return m_UserWidget;
}

void ConstantParameter::save ( std::queue<std::string>& saveInformation ) const
{
	saveInformation.push ( "Constant" );
	saveInformation.push ( m_SavedText );
}

std::unique_ptr<Parameter> ConstantParameter::load ( std::queue<std::string>& saveInformation, DelayedEventQueue& delayedEventQueue )
{
	std::unique_ptr<ConstantParameter> constant = std::make_unique<ConstantParameter> ( delayedEventQueue );
	//The line in saveInformation saying "Constant" has been removed by Parameter::load
	constant->m_SavedText = saveInformation.front();
	saveInformation.pop();
	constant->m_ValueEntry->SetText ( constant->m_SavedText );
	constant->UpdateValue();

	return constant;
}

TimeParameter::TimeParameter ( DelayedEventQueue& delayedEventQueue, std::vector<std::weak_ptr<std::function<void(float)>>>& updateQueue ) :
	m_Mode ( Mode::loop ), //Loop skips straight back to the start when it reaches the end, Oscillate goes back and forth.
	m_Playing ( true ), //TimeParameters should be playing as soon as they are created, and only paused when the user wants to.
	m_Start ( 0.0 ), //The initial value
	m_End ( 1.0 ), //The final value
	m_TimePeriod ( 1.0 ), //The time in seconds to get from m_Start to m_End
	m_Value ( 0.0 ), //The actual value
	m_Save { "0.0", "1.0", "1.0", 0 }, //The text that would exactly recreate the parameter if entered into its modification widget
	UpdateUserWidget ( delayedEventQueue, [ this ] { //Sync the user widget with the data held in the class.
		switch ( m_Mode ) {
		case Mode::loop:
			m_ValueLabel->SetText ( std::to_string ( m_Value ) );
			m_Bar->SetFraction ( ( m_Value - m_Start ) / ( m_End - m_Start ) );
			break;

		case Mode::oscillate:
			if ( m_Value < m_End ) {
				m_ValueLabel->SetText ( std::to_string ( m_Value ) );
				m_Bar->SetFraction ( ( m_Value - m_Start ) / ( m_End - m_Start ) );
			} else {
				m_ValueLabel->SetText ( std::to_string ( 2 * m_End - m_Value ) );
				m_Bar->SetFraction ( 1.0 - ( ( m_Value - m_End ) / ( m_End - m_Start ) ) );
			}
			break;
		}
	} ),
	UpdateValues ( delayedEventQueue, [ this ] { //Sync the text in the modification widget with the data held in the class.
		m_Save.start = m_StartEntry->GetText();
		m_Save.end = m_EndEntry->GetText();
		m_Save.time = m_TimeEntry->GetText();
		m_Save.mode = m_ModeComboBox->GetSelectedItem();

		m_Start = std::stod ( m_Save.start );
		m_End = std::stod ( m_Save.end );
		m_TimePeriod = std::stod ( m_Save.time );
		switch (m_ModeComboBox->GetSelectedItem () ) {
		case 0:
			m_Mode = Mode::loop;
			break;

		case 1:
			m_Mode = Mode::oscillate;
			break;
		}
	} )
{
	//The TimeParameter must be updated every frame so that it animates smoothly.
	//In order to allow a fixed or variable timestep, the timing is handled by gui, rather than TimeParameter.
	m_UpdateFunction = std::make_shared<std::function<void(float)>> ( [ this ] ( float time ) {
		if( m_Playing ) {
			m_Value += ( time / m_TimePeriod ) * ( m_End - m_Start );
			switch (m_Mode ) {
			case Mode::loop:
				while ( m_Value > m_End ) {
					m_Value -= m_End - m_Start;
				}
				break;

			case Mode::oscillate:
				while ( m_Value > 2 * m_End - m_Start) {
					m_Value -= 2 * ( m_End - m_Start );
				}
				break;
			}
			UpdateUserWidget();
		}
	} );

	updateQueue.push_back( m_UpdateFunction );

	//Modification widget - allows the user to tweak the Parameter.
	{
		auto title = sfg::Label::Create ( "Time parameter" );
		auto topSeparator = sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL );

		auto startLabel = sfg::Label::Create ( "Start" );
		m_StartEntry = sfg::Entry::Create ( "0.0" );

		auto endLabel = sfg::Label::Create ( "End" );
		m_EndEntry = sfg::Entry::Create ( "1.0" );

		auto timeLabel = sfg::Label::Create ( "Time" );
		m_TimeEntry = sfg::Entry::Create ( "1.0" );

		auto modeLabel = sfg::Label::Create ( "Mode" );
		m_ModeComboBox = sfg::ComboBox::Create();

		auto bottomSeparator = sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL );
		auto setButton = sfg::Button::Create ( "Set" );

		auto startBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
		startBox->Pack ( startLabel, false, false );
		startBox->Pack ( m_StartEntry, true, true );

		auto endBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
		endBox->Pack ( endLabel, false, false );
		endBox->Pack ( m_EndEntry, true, true );

		auto timeBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
		timeBox->Pack ( timeLabel, false, false );
		timeBox->Pack ( m_TimeEntry, true, true );

		auto modeBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
		modeBox->Pack ( modeLabel, false ,false );
		modeBox->Pack ( m_ModeComboBox, true, true );

		auto box = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
		box->Pack ( title, false, false );
		box->Pack ( topSeparator, false, true );
		box->Pack ( startBox, false, true );
		box->Pack ( endBox, false, true );
		box->Pack ( timeBox, false, true);
		box->Pack ( modeBox, false, true);
		box->Pack ( bottomSeparator, false, true);
		box->Pack ( setButton, false, true );

		m_ModificationWidget = box;

		Validator::ValidateEntry ( m_StartEntry, Validator::Trait::Decimal );
		Validator::ValidateEntry ( m_EndEntry, Validator::Trait::Decimal );
		Validator::ValidateEntry ( m_TimeEntry, { ui::Validator::Trait::Positive, ui::Validator::Trait::Decimal, ui::Validator::Trait::NonZero } );

		setButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ]  {
			UpdateValues();
		} );

		m_ModeComboBox->AppendItem ( "Loop" );
		m_ModeComboBox->AppendItem ( "Oscillate" );
		m_ModeComboBox->SelectItem ( 0 );
	}

	//User widget - allows the user to read the Parameter's value, as well as play or pause it.
	{
		m_ValueLabel = sfg::Label::Create ( "0.0" );
		m_Bar = sfg::ProgressBar::Create ( sfg::ProgressBar::Orientation::HORIZONTAL );

		auto playButton = sfg::Button::Create ( "Play" );
		auto pauseButton = sfg::Button::Create ( "Pause" );

		auto controlBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
		controlBox->Pack ( playButton, true, true );
		controlBox->Pack ( pauseButton, true, true );

		auto box = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
		box->Pack ( m_ValueLabel, false, false );
		box->Pack ( m_Bar, false, true );
		box->Pack ( controlBox, false, true );

		m_UserWidget = box;

		playButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] {
			m_Playing = true;
		} );

		pauseButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] {
			m_Playing = false;
		} );
	}
}

double TimeParameter::getValue() const
{
	if ( m_Value > m_End ) {
		return  2 * m_End - m_Value;
	} else {
		return m_Value;
	}
}

sfg::Widget::Ptr TimeParameter::getModificationWidget() const
{
	return m_ModificationWidget;
}

sfg::Widget::Ptr TimeParameter::getUserWidget() const
{
	return m_UserWidget;
}

void TimeParameter::setMode ( Mode mode )
{
	m_Mode = mode;
	m_ModeComboBox->SelectItem ( static_cast <int> ( mode ) );
}

void TimeParameter::save ( std::queue<std::string>& saveInformation ) const
{
	saveInformation.push ( "Time" );
	saveInformation.push ( m_Save.start );
	saveInformation.push ( m_Save.end );
	saveInformation.push ( m_Save.time );
	saveInformation.push ( std::to_string ( m_Save.mode ) );
	saveInformation.push ( std::to_string ( m_Playing ) );
}

std::unique_ptr<Parameter> TimeParameter::load ( std::queue<std::string>& saveInformation, DelayedEventQueue& delayedEventQueue, std::vector<std::weak_ptr<std::function<void(float)>>>& updateQueue )
{
	std::unique_ptr<TimeParameter> time = std::make_unique<TimeParameter> ( delayedEventQueue, updateQueue );

	//The line in saveInformation that contained "Time" has been removed by Parameter::load
	time->m_Save.start = saveInformation.front();
	saveInformation.pop();
	time->m_Save.end = saveInformation.front();
	saveInformation.pop();
	time->m_Save.time = saveInformation.front();
	saveInformation.pop();
	time->m_Save.mode = std::stoi ( saveInformation.front() );
	saveInformation.pop();
	time->m_Playing = std::stoul ( saveInformation.front() );
	saveInformation.pop();

	time->m_StartEntry->SetText ( time->m_Save.start );
	time->m_EndEntry->SetText ( time->m_Save.end );
	time->m_TimeEntry->SetText ( time->m_Save.time );
	time->m_ModeComboBox->SelectItem ( time->m_Save.mode );

	time->UpdateValues();
	time->UpdateUserWidget();

	return time;
}

} //END NAMESPACE ui
