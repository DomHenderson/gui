#ifndef LIBRARYWIDGET_HPP_INCLUDED
#define LIBRARYWIDGET_HPP_INCLUDED

//STANDARD LIBRARY INCLUDES
#include <functional>    //std::function
#include <queue>         //std::queue
#include <string>        //std::string
#include <unordered_map> //std::unordered

//SFGUI INCLUDES
#include <SFGUI/Box.hpp>     //sfg::Box
#include <SFGUI/Desktop.hpp> //sfg::Desktop
#include <SFGUI/Widget.hpp>  //sfg::Widget
#include <SFGUI/Window.hpp>  //sfg::Window

//EXPRESSION COMPILER INCLUDES
#include <ExpressionCompiler2/compiler.hpp> //ec::Compiler

//INTERNAL INCLUDES
#include "delayedFunction.hpp" //ui::DelayedEvent

namespace ui {

class LibraryWidget {
public:
	LibraryWidget ( DelayedEventQueue& delayedEventQueue, sfg::Desktop& desktop, ec::Compiler& compiler );

	void addConstant ( std::string name, double value );

	void addFunction ( std::string name, std::string expression, std::string variable );

	void reset ();
	void save ( std::queue<std::string>& saveInformation );
	void load ( std::queue<std::string>& saveInformation );

	sfg::Widget::Ptr getWidget() const;
private:
	struct Constant {
		double value;
		sfg::Box::Ptr widget;
	};

	struct Function {
		std::string expression;
		std::string name;
		std::string input;
		sfg::Box::Ptr widget;
	};

	Constant CreateConstant ( std::string name, double value );
	Function CreateFunction ( std::string name, std::string expression, std::string input );

	ec::Compiler& m_Compiler;

	std::map<std::string, Constant> m_Constants;
	std::map<std::string, Function> m_Functions;

	sfg::Box::Ptr m_MainBox;

	sfg::Box::Ptr m_ConstantsBox;
	sfg::Box::Ptr m_FunctionsBox;

	sfg::Window::Ptr m_AddConstantWindow;
	sfg::Window::Ptr m_AddFunctionWindow;

	DelayedEvent<std::string> RemoveConstant;
	DelayedEvent<std::string> RemoveFunction;
};

}

#endif // LIBRARYWIDGET_HPP_INCLUDED
