A graph drawing program with the following features:
Plotting cartesian (x->y and y->x) and polar (r->theta and theta->r) graphs
Symbolic Differentiation
Namespace style grouping of graphs and shared parameters
Parameters can be animated

![Screenshot1](images/Screenshot1-06-11-2018.png)
![Screenshot2](images/Screenshot2-06-11-2018.png)
![Screenshot3](images/Screenshot3-06-11-2018.png)

The code in this repository is dependant upon ExpressionCompiler2 and GraphCanvas.