#ifndef GUI_HPP_INCLUDED
#define GUI_HPP_INCLUDED

//STANDARD LIBRARY INCLUDES
#include <functional> // std::function
#include <string>     // std::string
#include <vector>     // std::vector

//SFML SYSTEM INCLUDES
#include <SFML/System/Clock.hpp> //sf::Clock

//SFML GRAPHICS INCLUDES
#include <SFML/Graphics/RenderWindow.hpp> //sf::RenderWindow

//SFGUI INCLUDES
#include <SFGUI/Box.hpp>      // sfg::Box
#include <SFGUI/Button.hpp>   // sfg::Button
#include <SFGUI/Canvas.hpp>   // sfg::Canvas
#include <SFGUI/ComboBox.hpp> // sfg::ComboBox
#include <SFGUI/Desktop.hpp>  // sfg::Desktop
#include <SFGUI/Entry.hpp>    // sfg::Entry
#include <SFGUI/Frame.hpp>    // sfg::Frame
#include <SFGUI/SFGUI.hpp>    // sfg::SFGUI
#include <SFGUI/Window.hpp>   // sfg::Window

//EXPRESSION COMPILER 2 INCLUDES
#include <ExpressionCompiler2/compiler.hpp> //ec::Compiler

//GRAPH CANVAS INCLUDES
#include <GraphCanvas/graphCanvas.hpp>   //gc::GraphCanvas

//INTERNAL INCLUDES
#include "delayedFunction.hpp"  //ui::DelayedEvent, ui::DelayedEventQueue
#include "libraryWidget.hpp"    //ui::LibraryWidget
#include "plotterPanel.hpp"     //ui::PlotterPanel
#include "zoomControl.hpp"      //ui::ZoomControl

namespace ui {

////////////////////////////////////////////////////////////////////////////////
/// \brief Encapsulates the entire GUI.
///
/// This is the only class from this library that needs to be used outside of
/// this library. All other classes are for use by this one, or for use by
/// others which are used by this one. The GUI contains 5 panels. The top panel
/// allows for switching of mode ( although this is not yet implemented ), and
/// also for changing of the colour scheme. The left panel is for declaring
/// functions. This allows the user to create many expressions, and then choose
/// which to display to the screen later. The bottom panel is for activating
/// functions, i.e. setting them to be drawn to the screen. This process is
/// by a window that is shown from the bottom panel. The right panel controls
/// modifying existing functions. Finally, the middle panel is where the graphs
/// will actually be drawn, when it is implemented.
////////////////////////////////////////////////////////////////////////////////
class Gui {

public:

	////////////////////////////////////////////////////////////////////////////
	/// \brief Constructor.
	///
	/// The Gui class does not depend on anything that it doesn't set up for
	/// itself, which is why the contructor takes no arguments.
	////////////////////////////////////////////////////////////////////////////
	Gui();

	////////////////////////////////////////////////////////////////////////////
	/// \brief Handle window events.
	///
	/// This function iterates through all events that are stored by the window,
	/// such as mouse moves, clicks, button presses etc. It then passes these
	/// events to SFGUI, which passes them through the Widget hierarchy. When
	/// some within the Widget hierarchy needs to be modified due to one of
	/// these events, DelayedEvents are used so that the modification is done
	/// while the Widgets are in a safe state.
	////////////////////////////////////////////////////////////////////////////
	void processEvents();

	////////////////////////////////////////////////////////////////////////////
	/// \brief Handles the passage of time.
	///
	/// Gui holds an internal clock, this event passes the amount of time since
	/// the previous frame to anything that requires this information. This
	/// includes Widgets and TimeParameters.
	////////////////////////////////////////////////////////////////////////////
	void update();

	////////////////////////////////////////////////////////////////////////////
	/// \brief Draw the GUI to the window.
	///
	/// Gui contains its own RenderWindow, so that it does not need to be set up
	/// and managed outside of the class. This function calls the clear draw
	/// display block that updates the contents of that window.
	////////////////////////////////////////////////////////////////////////////
	void draw();

	////////////////////////////////////////////////////////////////////////////
	/// \brief Returns whether or not the window is open.
	///
	/// This should be used to determine when to end the program. As such, it
	/// should be used in a while (gui.isOpen()) {DoStuff} loop.
	////////////////////////////////////////////////////////////////////////////
	bool isOpen();

private:

	enum class ColourScheme {
		Tron,
		TronAlt,
		Light,
		Night
	};

	DelayedEventQueue m_DelayedEventQueue;

	std::vector<std::weak_ptr<std::function<void ( float ) >>> m_UpdateQueue;

	ec::Compiler m_Compiler;

	//Splits up the constructor to make it more readable
	sfg::Frame::Ptr CreateLeftPanel();
	sfg::Frame::Ptr CreateRightPanel();
	sfg::Frame::Ptr CreateTopPanel();
	sfg::Frame::Ptr CreateBottomPanel();
	sfg::Frame::Ptr CreateMidPanel();
	void SetThemePreferences ( ColourScheme colourScheme );

	sfg::Desktop m_Desktop;
	sfg::SFGUI m_Sfgui;
	sfg::Window::Ptr m_MainWindow;
	sfg::ComboBox::Ptr m_ColourSchemeSelection;
	sfg::ComboBox::Ptr m_DRGSelector;

	LibraryWidget m_Library;

	ZoomControl m_ZoomControl;

	//Required in order to automate use
	sf::Clock m_Clock;
	sf::RenderWindow m_RenderWindow;

	//Delayed events can be called like normal functions,
	//however they have to be run separately before they actually take effect.
	//They are therefore used when tasks have to be sceduled to be done when
	//it is safe to do so.
	DelayedEvent<void> SwitchColourScheme;

	//Graph for the centre panel
	gc::GraphCanvas m_GraphCanvas;
	sf::Vector2f m_PixelsPerUnit;

	PlotterPanel m_PlotterPanel;

	sf::Color m_BackgroundColour;
};

} //END NAMESPACE ui

#endif // GUI_HPP_INCLUDED
