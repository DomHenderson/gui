#ifndef VALIDATOR_HPP_INCLUDED
#define VALIDATOR_HPP_INCLUDED

//STANDARD LIBRARY INCLUDES
#include <initializer_list> // std::initializer_list
#include <functional>       // std::function
#include <vector>           // std::vector

//SFGUI INCLUDES
#include <SFGUI/Entry.hpp> // sfg::Entry

////////////////////////////////////////////////////////////////////////////////
/// /brief Contains things useful for validating user input.
///
/// Validator contains that validate user input into Entries by giving the
/// Entires an Id of "Invalid" when they contain text that doesn't follow given
/// rules. It is not a class because it does not make sence to have to create
/// a Validator before using it.
////////////////////////////////////////////////////////////////////////////////
namespace ui::Validator {

////////////////////////////////////////////////////////////////////////////////
/// \brief Different types of recognised inputs.
///
/// Each item corresponds to an std::regex, that is tested against a the
/// contents of an Entry via std::regex_match, which is used because it tests
/// against the whole string rather than finding substrings which match.
////////////////////////////////////////////////////////////////////////////////
enum class Trait {
	////////////////////////////////////////////////////////////////////////////
	/// \brief Accepts any string composed of only letters.
	///
	/// This corresponds to the regex R"([a-zA-Z]+)" . It should be used to
	/// ensure that the input is a single word
	////////////////////////////////////////////////////////////////////////////
	Alphabetic,
	////////////////////////////////////////////////////////////////////////////
	/// \brief Accepts any number written in decimal form (including integers).
	///
	/// This corresponds to the regex R"([\+\-]?[0-9]+(\.[0-9]+)?)". It should
	/// be used if any number, integer or decimal is desired, as it would be
	/// stupid to force the user to write 0.0 instead of 0.
	////////////////////////////////////////////////////////////////////////////
	Decimal,
	////////////////////////////////////////////////////////////////////////////
	/// \brief Accepts any integer that does not contain separation.
	///
	/// This corresponds to the regex R"([\+\-]?[0-9]+)". It should be used
	/// for inputs that require a whole number.
	////////////////////////////////////////////////////////////////////////////
	Integer,
	////////////////////////////////////////////////////////////////////////////
	/// \brief Accepts anything beginning with a -.
	///
	/// This corresponds to the regex R"(\-.*)". It should be used in
	/// conjunction with another numeric trait, as it does not guarantee
	/// anything abotu the contents after the - sign.
	////////////////////////////////////////////////////////////////////////////
	Negative,
	////////////////////////////////////////////////////////////////////////////
	/// \brief Accepts anything other than the number 0.
	///
	/// This corresponds to the regex R"((?![\+\-]?0+\.?0*$).*)". It rejects
	/// both the integer and decimal ways of writing 0. The regex is relatively
	/// complex because it contains a negative lookahead, which is a way of
	/// checking that a pattern does not appear.
	////////////////////////////////////////////////////////////////////////////
	NonZero,
	////////////////////////////////////////////////////////////////////////////
	/// \brief Accepts any positive integer with a % sign appended.
	///
	/// This corresponds to the regex R"(\+?[0-9]+%)". It should most likely be
	/// used in conjunction with Integer, as accepting both 100% and 100 is more
	/// user friendly than insisting on the % sign.
	////////////////////////////////////////////////////////////////////////////
	Percentage,
	////////////////////////////////////////////////////////////////////////////
	/// \brief Accepts anything that doesn't begin with a -.
	///
	/// This corresponds to the regex R"([^\-].*)". It should be used in
	/// conjunction with another numeric Trait, because it does not check
	/// anything after the sign. Doing so also rejects ways of getting around
	/// this, such as +-1, which could either be considered to mean -1, or both
	/// + and -1. Neither is acceptable, but +-1 would not be rejected by this
	/// Trait. It would be rejected by both Integer and Decimal though.
	////////////////////////////////////////////////////////////////////////////
	Positive
};

////////////////////////////////////////////////////////////////////////////////
/// \brief Validates the text in the Entry.
///
/// This connects a function that checks the Entry's text against the passed
/// Traits to the Entry's OnTextChanged Signal. This function sets the Entry's
/// Id, which changes its appearance depending on the desktop styles.
///
/// \param entry An entry to which the validation rule will be applied.
/// \param trait An input category that determines whether the text is valid or
/// not.
////////////////////////////////////////////////////////////////////////////////
void ValidateEntry (
	sfg::Entry::Ptr entry,
	Trait           trait
);

////////////////////////////////////////////////////////////////////////////////
/// \brief Validates the text in the Entry.
///
/// This connects a function that checks the Entry's text against the passed
/// Traits to the Entry's OnTextChanged Signal. This function sets the Entry's
/// Id, which changes its appearance depending on the desktop styles.
///
/// \param entry An entry to which the validation rule will be applied.
/// \param traits A set of Traits, against which the Entry's text will be
/// compared. It's Id will only be set to "Valid" if it matches all of the
/// Traits.
////////////////////////////////////////////////////////////////////////////////
void ValidateEntry (
	sfg::Entry::Ptr              entry,
	std::initializer_list<Trait> traits
);

////////////////////////////////////////////////////////////////////////////////
///\brief Validates the text in the Entry.
///
/// This connects a function that checks the Entry's text against the passed
/// Traits to the Entry's OnTextChanged Signal. This function sets the Entry's
/// Id, which changes its appearance depending on the desktop styles.
///
/// \param entry An entry to which the validation rule will be applied.
/// \param traits A set of Traits against which the Entry's text will be
/// compared.
/// \param function A function that takes the results of comparing the Entry's
/// text against each Trait in the order they are passed in, that is used to
/// determine whether the results constitute a valid input or not. This should
/// be used if, for example, an input has to follow any other rule than adhering
/// to all Traits at all times.
////////////////////////////////////////////////////////////////////////////////
void ValidateEntry (
	sfg::Entry::Ptr                            entry,
	std::initializer_list<Trait>               traits,
	std::function<bool ( std::vector<bool> ) > function
);

} //END NAMESPACE ui::Validator

#endif // VALIDATOR_HPP_INCLUDED
