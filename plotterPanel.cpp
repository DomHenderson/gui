//STANDARD LIBRARY INCLUDES
#include <functional> //std::function
#include <map>        //std::map
#include <memory>     //std::make_unique, std::unique_ptr
#include <queue>      //std::queue
#include <set>        //std::set
#include <string>     //std::to_string, std::stod, std::stoul, std::string
#include <utility>    //std::bind, std::placeholders
#include <vector>     //std::vector

//SFGUI INCLUDES
#include <SFGUI/Box.hpp>            //sfg::Box
#include <SFGUI/Button.hpp>         //sfg::Button
#include <SFGUI/ComboBox.hpp>       //sfg::ComboBox
#include <SFGUI/Desktop.hpp>        //sfg::Desktop
#include <SFGUI/Frame.hpp>          //sfg::Frame
#include <SFGUI/Label.hpp>          //sfg::Label
#include <SFGUI/ScrolledWindow.hpp> //sfg::ScrolledWindow
#include <SFGUI/Separator.hpp>      //sfg::Separator
#include <SFGUI/Table.hpp>          //sfg::Table
#include <SFGUI/Widget.hpp>         //sfg::Widget
#include <SFGUI/Window.hpp>         //sfg::Window

//EXPRESSION COMPILER INCLUDES
#include <ExpressionCompiler2/compiler.hpp> //ec::Compiler

//GRAPH CANVAS INCLUDES
#include <GraphCanvas/plotter.hpp>     //gc::Plotter
#include <GraphCanvas/graphCanvas.hpp> //gc::GraphCanvas

//INTERNAL INCLUDES
#include "delayedFunction.hpp" //ui::DelayedEventQueue
#include "plotterPanel.hpp"    //ui::PlotterPanel
#include "validator.hpp"       //ui::Validator

namespace { //Convenient functions for use in this file

	//Gets the set of every key in a map.
	template <typename T, typename U>
	std::set<T> ExtractKeys ( std::map<T,U> map )
	{
		std::set<T> temp;
		for ( auto& x: map ) {
			temp.emplace ( x.first );
		}
		return temp;
	}

	//Gets all the items from a ComboBox and puts them in a set for easier manipulation.
	std::set<std::string> ExtractItems ( sfg::ComboBox::Ptr selector )
	{
		std::set<std::string> temp;
		for ( int i (0); i < selector->GetItemCount(); ++i ) {
			temp.emplace ( selector->GetItem ( i ) );
		}
		return temp;
	}
}

namespace ui {

PlotterPanel::PlotterPanel (
	sfg::Desktop& desktop,
	gc::GraphCanvas& graphCanvas,
	ec::Compiler& compiler,
	std::vector<std::weak_ptr<std::function<void(float)>>>& updateQueue,
	DelayedEventQueue& delayedEventQueue
) :
	m_CurrentPlotterName ( "Default" ), //This could be obtained from m_PlotterSelector, but it's much more convenient to simply be able to use a string.
	m_Compiler ( compiler ),
	m_GraphCanvas ( graphCanvas ),
	m_DelayedEventQueue ( delayedEventQueue ),
	m_UpdateQueue ( updateQueue ),
	m_PlotterSelector ( sfg::ComboBox::Create() ),
	m_NewPlotterButton ( sfg::Button::Create ( "New plotter" ) ),
	m_NewFunctionButton ( sfg::Button::Create ( "New function" ) ),
	m_NewDifferentialButton ( sfg::Button::Create ( "New differential" ) ),
	m_DeleteFunctionButton ( sfg::Button::Create ( "Delete function" ) ),
	m_DeletePlotterButton ( sfg::Button::Create ( "Delete plotter" ) ),
	m_FunctionView ( sfg::ScrolledWindow::Create () ),
	m_ParameterView ( sfg::ScrolledWindow::Create () ),
	m_NewPlotterWindow ( desktop, std::bind ( &PlotterPanel::AddPlotter, this, std::placeholders::_1, std::placeholders::_2 ) ),
	m_NewFunctionWindow (
		compiler,
		desktop,
		std::bind (
			&PlotterPanel::AddFunction,
			this,
			std::placeholders::_1 //functionInformation
		)
	),
	m_LinkingWindow ( //This window allows the user to create Parameters for all the extraneous variables.
		desktop,
		delayedEventQueue,
		updateQueue,
		std::bind (
			&PlotterPanel::AddParameter,
			this,
			std::placeholders::_1, //name
			std::placeholders::_2  //parameter
		)
	),
	m_DeleteFunctionWindow (
		desktop,
		std::bind (
			&PlotterPanel::DeleteFunction,
			this,
			std::placeholders::_1 //variables
		)
	),
	m_DeletePlotterWindow (
		desktop,
		std::bind (
			&PlotterPanel::DeletePlotter,
			this,
			std::placeholders::_1 //plotter name
		)
	),
	m_ParameterPanel (
		m_ParameterSets,
		m_CurrentPlotterName
	),
	m_NewDifferentialWindow (
		desktop,
		std::bind (
			&PlotterPanel::AddDifferential,
			this,
			std::placeholders::_1 //functionInformation
		)
	)
{
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//LAYOUTING
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	auto plotterBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	plotterBox->Pack ( m_PlotterSelector, true, true );
	plotterBox->Pack ( m_NewPlotterButton, true, true );

	auto leftBox = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
	leftBox->Pack ( sfg::Label::Create ( "Plotters" ), false, false );
	leftBox->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );
	leftBox->Pack ( plotterBox );
	leftBox->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );
	leftBox->Pack ( m_NewFunctionButton, false, true );
	leftBox->Pack ( m_NewDifferentialButton, false, true );
	leftBox->Pack ( m_DeleteFunctionButton, false, true );
	leftBox->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );
	leftBox->Pack ( m_DeletePlotterButton, false, true );

	auto mainBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	mainBox->Pack ( leftBox, false, true );
	mainBox->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::VERTICAL ), false, true );
	mainBox->Pack ( m_FunctionView, true, true );
	mainBox->Pack ( m_ParameterView, true, true );
	m_MainWidget = mainBox;

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//SET UP
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	m_FunctionView->SetScrollbarPolicy (
		sfg::ScrolledWindow::ScrollbarPolicy::HORIZONTAL_AUTOMATIC |
		sfg::ScrolledWindow::ScrollbarPolicy::VERTICAL_AUTOMATIC
	);

	m_ParameterView->SetScrollbarPolicy (
		sfg::ScrolledWindow::ScrollbarPolicy::HORIZONTAL_AUTOMATIC |
		sfg::ScrolledWindow::ScrollbarPolicy::VERTICAL_AUTOMATIC
	);

	m_PlotterSelector->AppendItem ( "Default" );
	m_PlotterSelector->SelectItem ( 0 );
	graphCanvas.addPlotter ( "Default", std::make_unique<gc::Plotter> ( "x" ) );

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//SIGNALS
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	m_PlotterSelector->GetSignal ( sfg::ComboBox::OnSelect ).Connect ( [ this ] () {
		m_CurrentPlotterName = m_PlotterSelector->GetSelectedText();
		RecreateFunctionView();
		RecreateParameterView();
	} );

	m_NewPlotterButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] () {
		m_NewPlotterWindow.Show();
	} );

	m_NewFunctionButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] () {
		m_NewFunctionWindow.Show();
	} );

	m_NewDifferentialButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] () {
		m_NewDifferentialWindow.Show ( ExtractKeys ( m_FunctionSets[m_CurrentPlotterName] ) );
	} );

	m_DeleteFunctionButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] () {
		if ( !m_FunctionSets [ m_CurrentPlotterName ].empty() ) {
			m_DeleteFunctionWindow.Show ( ExtractKeys ( m_FunctionSets[m_CurrentPlotterName] ) );
		}
	} );

	m_DeletePlotterButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] () {
		if ( m_PlotterSelector->GetItemCount() != 0 ) {
			m_DeletePlotterWindow.Show ( ExtractItems ( m_PlotterSelector ) );
		}
	} );

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//REAL TIME FUNCTIONS
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	m_UpdateFunction = std::make_shared<std::function<void(float)>> ( [ this ] ( float ) {
		for ( auto& x: m_ParameterSets ) {
			for ( auto& y: x.second ) {
				m_GraphCanvas.getPlotter ( x.first ).setVariable ( y.first, y.second->getValue() );
			}
		}
	} );
	updateQueue.emplace_back ( m_UpdateFunction );
}

void PlotterPanel::reset()
{
	for ( int i (0); i < m_PlotterSelector->GetItemCount(); ++i ) {
		m_GraphCanvas.removePlotter ( m_PlotterSelector->GetItem ( i ) );
	}
	m_PlotterSelector->Clear();
	m_FunctionSets.clear();
	m_ParameterSets.clear();

	m_CurrentPlotterName = "Default";
	m_PlotterSelector->AppendItem ( "Default" );
	m_PlotterSelector->SelectItem ( 0 );
	m_GraphCanvas.addPlotter( "Default", std::make_unique<gc::Plotter>("x"));
	RecreateFunctionView();
	RecreateParameterView();
	m_ParameterPanel.Update();

	//This gets deleted by the clearing of updateQueue by the new button
	m_UpdateQueue.emplace_back ( m_UpdateFunction );
}

void PlotterPanel::save ( std::queue<std::string>& saveInformation )
{
	saveInformation.push ( std::to_string ( m_PlotterSelector->GetItemCount() ) );
	for ( int i (0); i < m_PlotterSelector->GetItemCount(); ++i ) {
		std::string name = m_PlotterSelector->GetItem ( i );
		saveInformation.push ( name );
		saveInformation.push ( m_GraphCanvas.getPlotter ( name ).getIndependentVariable() );
		saveInformation.push ( std::to_string ( m_ParameterSets[name].size() ) );
		for ( auto& x: m_ParameterSets[name] ) {
			saveInformation.push ( x.first );
			x.second->save ( saveInformation );
		}
		saveInformation.push ( std::to_string ( m_FunctionSets[name].size() ) );
		for ( auto& x: m_FunctionSets[name] ) {
			saveInformation.push ( x.first );
			saveInformation.push ( x.second.expression );
			saveInformation.push ( std::to_string ( x.second.type ) );
			saveInformation.push ( std::to_string ( x.second.orientation ) );
			saveInformation.push ( std::to_string ( x.second.hasLowerBound ) );
			saveInformation.push ( std::to_string ( x.second.hasUpperBound ) );
			saveInformation.push ( std::to_string ( x.second.lowerBound ) );
			saveInformation.push ( std::to_string ( x.second.upperBound ) );
			saveInformation.push ( std::to_string ( x.second.minimumOut ) );
			saveInformation.push ( std::to_string ( x.second.maximumOut ) );
			saveInformation.push ( std::to_string ( x.second.pointGap ) );
			saveInformation.push ( std::to_string ( x.second.colour.r ) );
			saveInformation.push ( std::to_string ( x.second.colour.g ) );
			saveInformation.push ( std::to_string ( x.second.colour.b ) );
		}
	}
	saveInformation.push ( m_CurrentPlotterName );
}

void PlotterPanel::load ( std::queue<std::string>& saveInformation )
{
	//Reset the plotter to a blank state
	for ( int i (0); i < m_PlotterSelector->GetItemCount(); ++i ) {
		m_GraphCanvas.removePlotter ( m_PlotterSelector->GetItem ( i ) );
	}
	m_PlotterSelector->Clear();
	m_FunctionSets.clear();
	m_ParameterSets.clear();

	unsigned numOfPlotters = std::stoul ( saveInformation.front() );
	saveInformation.pop();
	for ( unsigned i ( 0 ); i < numOfPlotters; ++i ) {
		std::string name = saveInformation.front();
		saveInformation.pop();
		m_CurrentPlotterName = name;
		m_PlotterSelector->AppendItem ( name );
		std::string independentVar = saveInformation.front();
		saveInformation.pop();
		m_GraphCanvas.addPlotter ( name, std::make_unique<gc::Plotter> ( independentVar ) );

		unsigned numOfParameters = std::stoul ( saveInformation.front() );
		saveInformation.pop();
		for ( unsigned j ( 0 ); j < numOfParameters; ++j ) {
			std::string parameterName = saveInformation.front();
			saveInformation.pop();
			m_ParameterSets[name][parameterName] = ui::Parameter::load ( saveInformation, m_DelayedEventQueue, m_UpdateQueue );
		}

		unsigned numOfFunctions = std::stoul ( saveInformation.front() );
		saveInformation.pop();
		for ( unsigned j ( 0 ); j < numOfFunctions; ++j ) {
			FunctionInformation functionInformation;
			functionInformation.name = saveInformation.front();
			saveInformation.pop();
			functionInformation.expression = saveInformation.front();
			saveInformation.pop();
			functionInformation.type = std::stoul ( saveInformation.front() );
			saveInformation.pop();
			functionInformation.orientation = std::stoul ( saveInformation.front() );
			saveInformation.pop();
			functionInformation.hasLowerBound = std::stoul ( saveInformation.front() ); //Bools get stored as "0" or "1",
			saveInformation.pop();                                                      //but there is no string to bool conversion,
			functionInformation.hasUpperBound = std::stoul ( saveInformation.front() ); //however there is one from string to unsigned,
			saveInformation.pop();                                                      //and unsigned converts implicitly to bool.
			functionInformation.lowerBound = std::stod ( saveInformation.front() );
			saveInformation.pop();
			functionInformation.upperBound = std::stod ( saveInformation.front() );
			saveInformation.pop();
			functionInformation.minimumOut = std::stod ( saveInformation.front() );
			saveInformation.pop();
			functionInformation.maximumOut = std::stod ( saveInformation.front() );
			saveInformation.pop();
			functionInformation.pointGap = std::stod ( saveInformation.front() );
			saveInformation.pop();
			functionInformation.colour.r = std::stoul ( saveInformation.front() );
			saveInformation.pop();
			functionInformation.colour.g = std::stoul ( saveInformation.front() );
			saveInformation.pop();
			functionInformation.colour.b =std::stoul ( saveInformation.front() );
			saveInformation.pop();

			AddFunction ( functionInformation );
		}
	}
	m_CurrentPlotterName = saveInformation.front();
	saveInformation.pop();
	for ( int i (0); i < m_PlotterSelector->GetItemCount(); ++i ) {
		if ( m_PlotterSelector->GetItem ( i ) == m_CurrentPlotterName ) {
			m_PlotterSelector->SelectItem ( i );
		}
	}
	RecreateFunctionView();
	RecreateParameterView();
	m_ParameterPanel.Update();

	//This gets deleted by the clearing of updateQueue by the open button
	m_UpdateQueue.emplace_back ( m_UpdateFunction );
}

sfg::Widget::Ptr PlotterPanel::getWidget() const
{
	return m_MainWidget;
}

sfg::Widget::Ptr PlotterPanel::getParameterPanelWidget() const
{
	return m_ParameterPanel.getWidget();
}

void PlotterPanel::AddDifferential( FunctionInformation functionInformation )
{
	functionInformation.expression = m_FunctionSets [ m_CurrentPlotterName ] [ functionInformation.name + "'" ].expression = m_Compiler.differentiate (
		m_FunctionSets [ m_CurrentPlotterName ] [ functionInformation.name ].expression,
		m_GraphCanvas.getPlotter ( m_CurrentPlotterName ).getIndependentVariable()
	);
	functionInformation.name += "'";
	AddFunction ( functionInformation );
}

void PlotterPanel::AddFunction ( FunctionInformation functionInformation ) {
	m_FunctionSets[m_CurrentPlotterName] [ functionInformation.name ] = functionInformation;
	if ( functionInformation.type == 0 ) { //Cartesian
		if ( functionInformation.orientation == 0 ) {
			m_GraphCanvas.getPlotter ( m_CurrentPlotterName ).addCartesianFunction (
				functionInformation.name,
				m_Compiler.compile(functionInformation.expression),
				gc::CartesianFunction::PlottingMode::IndependentX
			);
		} else if ( functionInformation.orientation == 1 ) {
			m_GraphCanvas.getPlotter ( m_CurrentPlotterName ).addCartesianFunction (
				functionInformation.name,
				m_Compiler.compile(functionInformation.expression),
				gc::CartesianFunction::PlottingMode::IndependentY
			);
		}
	} else if ( functionInformation.type == 1 ) { //Polar
		if ( functionInformation.orientation == 0 ) {
			m_GraphCanvas.getPlotter ( m_CurrentPlotterName ).addPolarFunction (
				functionInformation.name,
				m_Compiler.compile(functionInformation.expression),
				gc::PolarFunction::PlottingMode::IndependentT
			);
		} else if ( functionInformation.orientation == 1 ) {
			m_GraphCanvas.getPlotter ( m_CurrentPlotterName ).addPolarFunction (
				functionInformation.name,
				m_Compiler.compile(functionInformation.expression),
				gc::PolarFunction::PlottingMode::IndependentR
			);
		}
	}
	gc::Function& function = m_GraphCanvas.getPlotter ( m_CurrentPlotterName ).getFunction( functionInformation.name );
	if ( functionInformation.hasLowerBound ) {
		function.setLowerBound ( functionInformation.lowerBound );
	}
	if ( functionInformation.hasUpperBound ) {
		function.setUpperBound ( functionInformation.upperBound );
	}
	if ( functionInformation.hasMinimumOut ) {
		function.setMinimumOut ( functionInformation.minimumOut );
	}
	if ( functionInformation.hasMaximumOut ) {
		function.setMaximumOut ( functionInformation.maximumOut );
	}
	function.setPointGap ( functionInformation.pointGap );
	function.setColour ( functionInformation.colour );
	m_GraphCanvas.setGraphArea( m_GraphCanvas.getGraphArea() );

	std::set<std::string> variables = m_Compiler.getVariableNames ( functionInformation.expression );
	variables.erase ( m_GraphCanvas.getPlotter ( m_CurrentPlotterName ).getIndependentVariable() );
	for ( auto& x: m_ParameterSets[m_CurrentPlotterName] ) {
		variables.erase ( x.first );
	}

	if ( !variables.empty() ) {
		m_LinkingWindow.Show ( variables );
	}

	RecreateFunctionView();
	RecreateParameterView();
}

void PlotterPanel::AddParameter ( std::string name, std::unique_ptr<Parameter> parameter )
{
	m_ParameterSets[m_CurrentPlotterName][name] = std::move ( parameter );
	RecreateParameterView();
}

void PlotterPanel::AddPlotter ( std::string name, std::string var )
{
	m_PlotterSelector->AppendItem ( name );
	m_GraphCanvas.addPlotter ( name, std::make_unique<gc::Plotter> ( var ) );
	m_PlotterSelector->SelectItem ( m_PlotterSelector->GetItemCount() - 1 );
	m_CurrentPlotterName = name;
	RecreateFunctionView();
	RecreateParameterView();
}

void PlotterPanel::DeleteFunction ( std::string name )
{
	m_FunctionSets[m_CurrentPlotterName].erase( name );
	m_GraphCanvas.getPlotter ( m_CurrentPlotterName ).removeFunction ( name );
	RecreateFunctionView();
	RecreateParameterView();
}

void PlotterPanel::DeletePlotter ( std::string name )
{
	m_DelayedEventQueue.clear();
	m_FunctionSets.erase ( name );
	m_ParameterSets.erase ( name );
	m_GraphCanvas.removePlotter ( name );
	for ( int i ( 0 ); i < m_PlotterSelector->GetItemCount(); ++i ) {
		if ( m_PlotterSelector->GetItem ( i ) == name ) {
			m_PlotterSelector->RemoveItem ( i );
		}
	}
	if ( m_PlotterSelector->GetItemCount() == 0 ) {
		m_PlotterSelector->AppendItem ( "Default" );
		m_GraphCanvas.addPlotter ( "Default", std::make_unique<gc::Plotter>("x") );
	}
	m_PlotterSelector->SelectItem ( 0 );
	m_CurrentPlotterName = m_PlotterSelector->GetItem ( 0 );
	RecreateFunctionView();
	RecreateParameterView();

}

void PlotterPanel::RecreateFunctionView()
{
	auto box = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
	for ( auto& x: m_FunctionSets [ m_CurrentPlotterName ] ) {
		box->Pack (
			sfg::Label::Create (
				x.first + " : " + m_GraphCanvas.getPlotter ( m_CurrentPlotterName ).getIndependentVariable() + " -> " + x.second.expression
			),
			false,
			false
		);
	}

	m_FunctionView->RemoveAll();
	m_FunctionView->AddWithViewport ( box );
}

void PlotterPanel::RecreateParameterView()
{
	auto mainBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	for ( auto& x: m_ParameterSets ) {
		for ( auto& y: x.second ) {
			y.second->getUserWidget()->Show ( false );
		}
	}
	for ( auto& x: m_ParameterSets [ m_CurrentPlotterName ] ) {
		auto box = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
		box->Pack ( sfg::Label::Create ( x.first ), false, false );
		box->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );
		box->Pack ( x.second->getUserWidget(), true, true );
		x.second->getUserWidget()->Show(true);

		auto frame = sfg::Frame::Create ();
		frame->Add ( box );
		mainBox->Pack ( frame, true, true );
	}
	m_ParameterView->RemoveAll();
	m_ParameterView->AddWithViewport ( mainBox );
	m_ParameterPanel.Update();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//NEW PLOTTER WINDOW
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
PlotterPanel::NewPlotterWindow::NewPlotterWindow ( sfg::Desktop& desktop, std::function<void(std::string,std::string)> callback ) :
	m_Desktop ( desktop ),
	m_Callback ( callback ),
	m_Window ( sfg::Window::Create() ),
	m_NameEntry ( sfg::Entry::Create() ),
	m_VarEntry ( sfg::Entry::Create() ),
	m_CancelButton ( sfg::Button::Create( "Cancel" ) ),
	m_DoneButton ( sfg::Button::Create( "Done" ) )
{
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//LAYOUTING
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	auto box = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
	box->Pack ( sfg::Label::Create ( "Register a new plotter" ), false, false );
	box->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ) );

	auto nameBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	nameBox->Pack ( sfg::Label::Create ( "Name" ), false, false );
	nameBox->Pack ( m_NameEntry, true, true );
	box->Pack ( nameBox, false, true );

	auto varBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	varBox->Pack ( sfg::Label::Create ( "Independent variable" ), false, false );
	varBox->Pack ( m_VarEntry, true, true );
	m_VarEntry->SetRequisition ( { 50.f, 0 } );
	box->Pack ( varBox, false, true );
	box->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );

	auto buttonBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	buttonBox->Pack ( m_CancelButton, true, true );
	buttonBox->Pack ( m_DoneButton, true, true );
	box->Pack ( buttonBox, false, true );

	m_Window->Add ( box );
	m_Desktop.Add ( m_Window );
	m_Window->Show ( false );

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//SIGNALS
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	m_CancelButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] () {
		Hide();
	} );

	m_DoneButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] () {
		m_Callback ( m_NameEntry->GetText(), m_VarEntry->GetText() );
		Hide();
	} );
}

void PlotterPanel::NewPlotterWindow::Show ()
{
	m_Window->Show ( true );
	m_Desktop.BringToFront ( m_Window );
}

void PlotterPanel::NewPlotterWindow::Hide ()
{
	m_NameEntry->SetText ( "" );
	m_VarEntry->SetText ( "" );
	m_Window->Show ( false );
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//NEW FUNCTION WINDOW
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
PlotterPanel::NewFunctionWindow::NewFunctionWindow (
	ec::Compiler& compiler,
	sfg::Desktop& desktop,
	std::function<void( FunctionInformation functionInformation )> callback
) :
	m_Compiler ( compiler ),
	m_Desktop ( desktop ),
	m_Callback ( callback ),
	m_HasLowerBound ( false ),
	m_HasUpperBound ( false ),
	m_HasMinimumOut ( false ),
	m_HasMaximumOut ( false ),
	m_Window ( sfg::Window::Create () ),
	m_NameEntry ( sfg::Entry::Create () ),
	m_ExpressionEntry ( sfg::Entry::Create () ),
	m_TypeSelector ( sfg::ComboBox::Create () ),
	m_OrientationSelector ( sfg::ComboBox::Create () ),
	m_LowerBoundToggle ( sfg::Button::Create ( "Add" ) ),
	m_UpperBoundToggle ( sfg::Button::Create ( "Add" ) ),
	m_LowerBoundEntry ( sfg::Entry::Create ( "-100" ) ),
	m_UpperBoundEntry ( sfg::Entry::Create ( "100" ) ),
	m_MinimumOutToggle ( sfg::Button::Create ( "Add" ) ),
	m_MaximumOutToggle ( sfg::Button::Create ( "Add" ) ),
	m_MinimumEntry ( sfg::Entry::Create ( "-1000" ) ),
	m_MaximumEntry ( sfg::Entry::Create ( "1000" ) ),
	m_PointGapToggle ( sfg::Button::Create ( "Custom" ) ),
	m_PointGapEntry ( sfg::Entry::Create ( "0.02" ) ),
	m_ColourSelector ( sfg::ComboBox::Create() ),
	m_RedEntry ( sfg::Entry::Create ( "255" ) ),
	m_GreenEntry ( sfg::Entry::Create ( "255" ) ),
	m_BlueEntry ( sfg::Entry::Create ( "255" ) ),
	m_CancelButton ( sfg::Button::Create ( "Cancel" ) ),
	m_DoneButton ( sfg::Button::Create ( "Done" ) )
{
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//LAYOUTING
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	auto box = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
	box->Pack ( sfg::Label::Create ( "Register a new function" ), false, false );
	box->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );

	auto nameBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	nameBox->Pack ( sfg::Label::Create ( "Name" ), false, false );
	nameBox->Pack ( m_NameEntry, true, true );
	box->Pack ( nameBox, false, true );

	auto expressionBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	expressionBox->Pack ( sfg::Label::Create ( "Expression" ), false, false );
	expressionBox->Pack ( m_ExpressionEntry, true, true );
	m_ExpressionEntry->SetRequisition ( {200.f, 0.f} );
	box->Pack ( expressionBox, false, true );
	box->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );

	auto typeBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	typeBox->Pack ( sfg::Label::Create ( "Type" ), false, false );
	typeBox->Pack ( m_TypeSelector, true, true );
	typeBox->Pack ( m_OrientationSelector, true, true );
	box->Pack ( typeBox, false, true );
	box->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );

	auto lowerBoundBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	lowerBoundBox->Pack ( sfg::Label::Create ( "Lower bound" ), false, false );
	lowerBoundBox->Pack ( m_LowerBoundToggle, true, true );
	box->Pack ( lowerBoundBox, false, true );
	box->Pack ( m_LowerBoundEntry, false, true );

	auto upperBoundBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	upperBoundBox->Pack ( sfg::Label::Create ( "Upper bound" ), false, false );
	upperBoundBox->Pack ( m_UpperBoundToggle, true, true );
	box->Pack ( upperBoundBox, false, true );
	box->Pack ( m_UpperBoundEntry, false, true );

	auto minimumBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	minimumBox->Pack ( sfg::Label::Create ( "Minimum output" ), false, false );
	minimumBox->Pack ( m_MinimumOutToggle, true, true );
	box->Pack ( minimumBox, false, true );
	box->Pack ( m_MinimumEntry, false, true );

	auto maximumBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	maximumBox->Pack ( sfg::Label::Create ( "Maximum output" ), false, false );
	maximumBox->Pack ( m_MaximumOutToggle, true, true );
	box->Pack ( maximumBox, false, true );
	box->Pack ( m_MaximumEntry, false, true );

	auto pointGapBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	pointGapBox->Pack ( sfg::Label::Create ( "Point gap" ), false, false );
	pointGapBox->Pack ( m_PointGapToggle, true, true );
	box->Pack ( pointGapBox, false, true );
	box->Pack ( m_PointGapEntry, false, true );
	box->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );

	auto colourSelectorBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	colourSelectorBox->Pack ( sfg::Label::Create ( "Colour" ), false, false );
	colourSelectorBox->Pack ( m_ColourSelector, true, true );
	box->Pack ( colourSelectorBox, false, true );

	auto redBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	redBox->Pack ( sfg::Label::Create ( "Red" ), false, false );
	redBox->Pack ( m_RedEntry, true, true );
	box->Pack ( redBox, false, true );

	auto greenBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	greenBox->Pack ( sfg::Label::Create ( "Green" ), false, false );
	greenBox->Pack ( m_GreenEntry, true, true );
	box->Pack ( greenBox, false, true );

	auto blueBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	blueBox->Pack ( sfg::Label::Create ( "Blue" ), false, false );
	blueBox->Pack ( m_BlueEntry, true, true );
	box->Pack ( blueBox, false, true );
	box->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );

	auto buttonBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	buttonBox->Pack ( m_CancelButton, true, true );
	buttonBox->Pack ( m_DoneButton, true, true );
	box->Pack ( buttonBox, false, true );

	m_Window->Add ( box );
	m_Desktop.Add ( m_Window );
	m_Window->Show ( false );
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//SET UP
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	m_TypeSelector->AppendItem ( "Cartesian" );
	m_TypeSelector->AppendItem ( "Polar" );
	m_TypeSelector->SelectItem ( 0 );

	m_OrientationSelector->AppendItem ( "Horizontal -> Vertical" );
	m_OrientationSelector->AppendItem ( "Vertical -> Horizontal" );
	m_OrientationSelector->SelectItem ( 0 );

	m_LowerBoundEntry->Show ( false );
	m_UpperBoundEntry->Show ( false );
	Validator::ValidateEntry ( m_LowerBoundEntry, Validator::Trait::Decimal );
	Validator::ValidateEntry ( m_UpperBoundEntry, Validator::Trait::Decimal );

	m_MinimumEntry->Show ( false );
	m_MaximumEntry->Show ( false );
	Validator::ValidateEntry ( m_MinimumEntry, Validator::Trait::Decimal );
	Validator::ValidateEntry ( m_MaximumEntry, Validator::Trait::Decimal );

	m_PointGapEntry->Show ( false );
	Validator::ValidateEntry ( m_PointGapEntry, { Validator::Trait::Decimal, Validator::Trait::Positive, Validator::Trait::NonZero } );

	m_ColourSelector->AppendItem ( "White" );
	m_ColourSelector->AppendItem ( "Red" );
	m_ColourSelector->AppendItem ( "Orange" );
	m_ColourSelector->AppendItem ( "Yellow" );
	m_ColourSelector->AppendItem ( "Green" );
	m_ColourSelector->AppendItem ( "Cyan" );
	m_ColourSelector->AppendItem ( "Blue" );
	m_ColourSelector->AppendItem ( "Purple" );
	m_ColourSelector->AppendItem ( "Pink" );
	m_ColourSelector->AppendItem ( "Custom" );
	m_ColourSelector->SelectItem ( 0 );

	redBox->Show ( false );
	greenBox->Show ( false );
	blueBox->Show ( false );

	m_Window->SetAllocation ( { 0.f, 0.f, 0.f, 0.f } );

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//SIGNALS
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	m_TypeSelector->GetSignal ( sfg::ComboBox::OnSelect ).Connect ( [ this ] () {
		m_OrientationSelector->Clear();
		if ( m_TypeSelector->GetSelectedItem() == 0 ) { //Cartesian
			m_OrientationSelector->AppendItem ( "Horizontal -> Vertical" );
			m_OrientationSelector->AppendItem ( "Vertical -> Horizontal" );
			m_OrientationSelector->SelectItem ( 0 );
		} else if ( m_TypeSelector->GetSelectedItem() == 1 ) { //Polar
			m_OrientationSelector->AppendItem ( "Angular -> Radial" );
			m_OrientationSelector->AppendItem ( "Radial -> Angular" );
			m_OrientationSelector->SelectItem ( 0 );
		}
	} );

	m_LowerBoundToggle->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] () {
		if ( m_HasLowerBound ) {
			m_LowerBoundToggle->SetLabel ( "Add" );
			m_LowerBoundEntry->Show ( false );
			m_HasLowerBound = false;
		} else {
			m_LowerBoundToggle->SetLabel ( "Remove" );
			m_LowerBoundEntry->Show ( true );
			m_HasLowerBound = true;
		}
		m_Window->SetAllocation ( { 0.f, 0.f, 0.f, 0.f } );
	} );

	m_UpperBoundToggle->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] () {
		if ( m_HasUpperBound ) {
			m_UpperBoundToggle->SetLabel ( "Add" );
			m_UpperBoundEntry->Show ( false );
			m_HasUpperBound = false;
		} else {
			m_UpperBoundToggle->SetLabel ( "Remove" );
			m_UpperBoundEntry->Show ( true );
			m_HasUpperBound = true;
		}
		m_Window->SetAllocation ( { 0.f, 0.f, 0.f, 0.f } );
	} );

	m_MinimumOutToggle->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] () {
		if ( m_HasMinimumOut ) {
			m_MinimumOutToggle->SetLabel ( "Add" );
			m_MinimumEntry->Show ( false );
			m_HasMinimumOut = false;
		} else {
			m_MinimumOutToggle->SetLabel ( "Remove" );
			m_MinimumEntry->Show ( true );
			m_HasMinimumOut = true;
		}
		m_Window->SetAllocation ( { 0.f, 0.f, 0.f, 0.f } );
	} );

	m_MaximumOutToggle->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] () {
		if ( m_HasMaximumOut ) {
			m_MaximumOutToggle->SetLabel ( "Add" );
			m_MaximumEntry->Show ( false );
			m_HasMaximumOut = false;
		} else {
			m_MaximumOutToggle->SetLabel ( "Remove" );
			m_MaximumEntry->Show ( true );
			m_HasMaximumOut = true;
		}
		m_Window->SetAllocation ( { 0.f, 0.f, 0.f, 0.f } );
	} );

	m_PointGapToggle->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] () {
		if ( m_PointGapToggle->GetLabel() == "Custom" ) {
			m_PointGapToggle->SetLabel ( "Default" );
			m_PointGapEntry->Show ( true );
		} else {
			m_PointGapToggle->SetLabel ( "Custom" );
			m_PointGapEntry->Show ( false );
			m_PointGapEntry->SetText ( "0.02" );
		}
		m_Window->SetAllocation ( { 0.f, 0.f, 0.f, 0.f } );
	} );

	m_ColourSelector->GetSignal ( sfg::ComboBox::OnSelect ).Connect ( [ this, redBox, greenBox, blueBox ] () {
		switch ( m_ColourSelector->GetSelectedItem() ) {
		case 0: //White
			m_RedEntry->SetText ( "255" );
			m_GreenEntry->SetText ( "255" );
			m_BlueEntry->SetText ( "255" );

			redBox->Show ( false );
			greenBox->Show ( false );
			blueBox->Show ( false );
			break;

		case 1: //Red
			m_RedEntry->SetText ( "255" );
			m_GreenEntry->SetText ( "0" );
			m_BlueEntry->SetText ( "0" );

			redBox->Show ( false );
			greenBox->Show ( false );
			blueBox->Show ( false );
			break;

		case 2: //Orange
			m_RedEntry->SetText ( "255" );
			m_GreenEntry->SetText ( "128" );
			m_BlueEntry->SetText ( "0" );

			redBox->Show ( false );
			greenBox->Show ( false );
			blueBox->Show ( false );
			break;

		case 3: //Yellow
			m_RedEntry->SetText ( "255" );
			m_GreenEntry->SetText ( "255" );
			m_BlueEntry->SetText ( "0" );

			redBox->Show ( false );
			greenBox->Show ( false );
			blueBox->Show ( false );
			break;

		case 4: //Green
			m_RedEntry->SetText ( "0" );
			m_GreenEntry->SetText ( "255" );
			m_BlueEntry->SetText ( "0" );

			redBox->Show ( false );
			greenBox->Show ( false );
			blueBox->Show ( false );
			break;

		case 5: //Cyan
			m_RedEntry->SetText ( "0" );
			m_GreenEntry->SetText ( "255" );
			m_BlueEntry->SetText ( "255" );

			redBox->Show ( false );
			greenBox->Show ( false );
			blueBox->Show ( false );
			break;

		case 6: //Blue
			m_RedEntry->SetText ( "0" );
			m_GreenEntry->SetText ( "0" );
			m_BlueEntry->SetText ( "255" );

			redBox->Show ( false );
			greenBox->Show ( false );
			blueBox->Show ( false );
			break;

		case 7: //Purple
			m_RedEntry->SetText ( "128" );
			m_GreenEntry->SetText ( "0" );
			m_BlueEntry->SetText ( "128" );

			redBox->Show ( false );
			greenBox->Show ( false );
			blueBox->Show ( false );
			break;

		case 8: //Pink
			m_RedEntry->SetText ( "255" );
			m_GreenEntry->SetText ( "0" );
			m_BlueEntry->SetText ( "255" );

			redBox->Show ( false );
			greenBox->Show ( false );
			blueBox->Show ( false );
			break;

		case 9: //Custom
			redBox->Show ( true );
			greenBox->Show ( true );
			blueBox->Show ( true );
			break;
		}
		m_Window->SetAllocation ( { 0.f, 0.f, 0.f, 0.f } );
	} );

	m_CancelButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] () {
		Hide();
	} );

	m_DoneButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] () {
		std::string lowerBound ( "0.0" );
		std::string upperBound ( "0.0" );
		std::string minimumOut ( "0.0" );
		std::string maximumOut ( "0.0" );
		if ( m_HasLowerBound ) {
			lowerBound = m_LowerBoundEntry->GetText();
		}
		if ( m_HasUpperBound ) {
			upperBound = m_UpperBoundEntry->GetText();
		}
		if ( m_HasMinimumOut ) {
			minimumOut = m_MinimumEntry->GetText();
		}
		if ( m_HasMaximumOut ) {
			maximumOut = m_MaximumEntry->GetText();
		}
		m_Callback (
			FunctionInformation {
				m_NameEntry->GetText(),
				m_ExpressionEntry->GetText(),
				static_cast<unsigned> (m_TypeSelector->GetSelectedItem()), //Although GetSelectedItem returns an int, it has been
				static_cast<unsigned> (m_OrientationSelector->GetSelectedItem()), //set up so that it can never be negative.
				m_HasLowerBound,
				m_HasUpperBound,
				std::stod ( lowerBound ),
				std::stod ( upperBound ),
				m_HasMinimumOut,
				m_HasMaximumOut,
				std::stod ( minimumOut ),
				std::stod ( maximumOut ),
				std::stod ( std::string (m_PointGapEntry->GetText() ) ),
				sf::Color (
					std::stoul ( std::string ( m_RedEntry->GetText() ) ),
					std::stoul ( std::string ( m_GreenEntry->GetText() ) ),
					std::stoul ( std::string ( m_BlueEntry->GetText() ) )
				)
			}
		);
		Hide();
	} );
}

void PlotterPanel::NewFunctionWindow::Show ( )
{
	m_Window->Show ( true );
	m_Desktop.BringToFront ( m_Window );
}

void PlotterPanel::NewFunctionWindow::Hide()
{
	m_NameEntry->SetText("");
	m_ExpressionEntry->SetText("");
	m_Window->Show ( false );
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//LINKING WINDOW
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
PlotterPanel::LinkingWindow::LinkingWindow (
	sfg::Desktop& desktop,
	DelayedEventQueue& delayedEventQueue,
	std::vector<std::weak_ptr<std::function<void(float)>>>& updateQueue,
	std::function<void(std::string, std::unique_ptr<Parameter>)> callback
) :
	m_Desktop ( desktop ),
	m_DelayedEventQueue ( delayedEventQueue ),
	m_UpdateQueue ( updateQueue ),
	m_Callback ( callback ),
	m_Window ( sfg::Window::Create() ),
	m_ParameterBox ( sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f ) ),
	m_CancelButton ( sfg::Button::Create ( "Cancel" ) ),
	m_DoneButton ( sfg::Button::Create ( "Done" ) )
{
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//LAYOUTING
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	auto box = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
	box->Pack ( sfg::Label::Create ( "Register variables" ) );
	box->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );
	box->Pack ( m_ParameterBox, false, true );
	box->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );
	auto buttonBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	buttonBox->Pack ( m_CancelButton, true, true );
	buttonBox->Pack ( m_DoneButton, true, true );
	box->Pack ( buttonBox, false, true );
	m_Window->Add ( box );
	m_Desktop.Add ( m_Window );
	m_Window->Show ( false );

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//SIGNALS
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	m_CancelButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] () {
		Hide();
	} );

	m_DoneButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] () {
		for ( auto& x: m_Parameters ) {
			switch ( x.second ) {
			case 0: //Slider
				m_Callback ( x.first, std::make_unique<SliderParameter> ( m_DelayedEventQueue ) );
				break;

			case 1: //Constant
				m_Callback ( x.first, std::make_unique<ConstantParameter> ( m_DelayedEventQueue ) );
				break;

			case 2: //Timer
				m_Callback ( x.first, std::make_unique<TimeParameter> ( m_DelayedEventQueue, m_UpdateQueue ) );
				break;
			}
		}
		Hide();
	} );
}

void PlotterPanel::LinkingWindow::Show( std::set<std::string> names )
{
	m_Parameters.clear();
	m_ParameterBox->RemoveAll();
	for ( auto& x: names ) {
		auto box = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
		box->Pack ( sfg::Label::Create ( x ), false, false );
		auto selector = sfg::ComboBox::Create ();
		box->Pack ( selector, true, true );
		selector->AppendItem ( "Slider" );
		selector->AppendItem ( "Constant" );
		selector->AppendItem ( "Timer" );
		selector->SelectItem ( 0 );
		m_Parameters[x] = 0;
		std::weak_ptr<sfg::ComboBox> selectorPtr = selector;
		selector->GetSignal ( sfg::ComboBox::OnSelect ).Connect ( [ this, x, selectorPtr ] () {
			if ( auto ptr = selectorPtr.lock() ) {
				m_Parameters [ x ] = ptr->GetSelectedItem();
			}
		} );
		m_ParameterBox->Pack ( box, false, true );
	}
	m_Window->Show ( true );
	m_Window->SetAllocation ( {0,0,0,0} );
	m_Desktop.BringToFront ( m_Window );
}

void PlotterPanel::LinkingWindow::Hide()
{
	m_Window->Show ( false );
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//DELETE FUNCTION WINDOW
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
PlotterPanel::DeleteFunctionWindow::DeleteFunctionWindow (
	sfg::Desktop& desktop,
	std::function<void(std::string)> callback
) :
	m_Desktop ( desktop ),
	m_Callback ( callback ),
	m_Window ( sfg::Window::Create () ),
	m_Selector ( sfg::ComboBox::Create() ),
	m_CancelButton ( sfg::Button::Create ( "Cancel" ) ),
	m_DoneButton ( sfg::Button::Create ( "Done" ) )
{

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//LAYOUTING
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	auto box = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
	box->Pack ( sfg::Label::Create ( "Delete a function" ), false, false );
	box->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );

	auto midBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	midBox->Pack ( sfg::Label::Create ( "Function to remove: " ), false, false );
	midBox->Pack ( m_Selector, true, true );
	box->Pack ( midBox, false, true );
	box->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );

	auto buttonBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	buttonBox->Pack ( m_CancelButton, true, true );
	buttonBox->Pack ( m_DoneButton, true, true );
	box->Pack ( buttonBox, false, true );

	m_Window->Add ( box );
	m_Desktop.Add ( m_Window );
	m_Window->Show ( false );

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//SIGNALS
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	m_CancelButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] () {
		Hide();
	} );

	m_DoneButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] () {
		m_Callback ( m_Selector->GetSelectedText() );
		Hide();
	} );
}

void PlotterPanel::DeleteFunctionWindow::Show ( std::set<std::string> functionNames )
{
	for ( auto& x: functionNames ) {
		m_Selector->AppendItem ( x );
	}
	m_Selector->SelectItem ( 0 );
	m_Window->Show ( true );
	m_Desktop.BringToFront ( m_Window );
}

void PlotterPanel::DeleteFunctionWindow::Hide ()
{
	m_Selector->Clear();
	m_Window->Show ( false );
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//DELETE PLOTTER WINDOW
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
PlotterPanel::DeletePlotterWindow::DeletePlotterWindow (
	sfg::Desktop& desktop,
	std::function<void(std::string)> callback
) :
	m_Desktop ( desktop ),
	m_Callback ( callback ),
	m_Window ( sfg::Window::Create() ),
	m_Selector ( sfg::ComboBox::Create () ),
	m_CancelButton ( sfg::Button::Create ( "Cancel" ) ),
	m_DoneButton ( sfg::Button::Create ( "Done" ) )
{
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//LAYOUTING
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	auto box = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL , 5.f );
	box->Pack ( sfg::Label::Create ( "Delete a plotter" ), false, false );
	box->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );

	auto midBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	midBox->Pack ( sfg::Label::Create ( "Plotter to delete: " ), false, false );
	midBox->Pack ( m_Selector, false, false );
	box->Pack ( midBox, false, true );
	box->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );

	auto buttonBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	buttonBox->Pack ( m_CancelButton, true, true );
	buttonBox->Pack ( m_DoneButton, true, true );
	box->Pack ( buttonBox, false, false );

	m_Window->Add ( box );
	m_Desktop.Add ( m_Window );
	m_Window->Show ( false );

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//SIGNALS
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	m_CancelButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] () {
		Hide();
	} );

	m_DoneButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] () {
		std::cout << "Deleting "<< std::string(m_Selector->GetSelectedText())<<std::endl;
		m_Callback ( m_Selector->GetSelectedText() );
		Hide();
	} );
}

void PlotterPanel::DeletePlotterWindow::Show ( std::set<std::string> plotterNames )
{
	for ( auto& x: plotterNames ) {
		m_Selector->AppendItem ( x );
	}
	m_Selector->SelectItem(0);
	m_Window->Show ( true );
	m_Desktop.BringToFront ( m_Window );
}

void PlotterPanel::DeletePlotterWindow::Hide ()
{
	m_Selector->Clear();
	m_Window->Show ( false );
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//PARAMETER PANEL
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
PlotterPanel::ParameterPanel::ParameterPanel (
	std::map<std::string, std::map<std::string, std::unique_ptr<Parameter>>>& parameters,
	std::string& currentPlotterName
) :
	m_Parameters ( parameters ),
	m_CurrentPlotterName ( currentPlotterName ),
	m_MainBox ( sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f ) ),
	m_ParameterBox ( sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f ) )
{
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//LAYOUTING
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	m_MainBox->Pack ( sfg::Label::Create ( "Parameter modification" ), false, false );
	m_MainBox->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );

	auto scrolledWindow = sfg::ScrolledWindow::Create ();
	scrolledWindow->AddWithViewport ( m_ParameterBox );
	m_MainBox->Pack ( scrolledWindow, true, true );

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//SET UP
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	scrolledWindow->SetScrollbarPolicy (
		sfg::ScrolledWindow::ScrollbarPolicy::HORIZONTAL_NEVER |
		sfg::ScrolledWindow::ScrollbarPolicy::VERTICAL_AUTOMATIC
	);
}

sfg::Widget::Ptr PlotterPanel::ParameterPanel::getWidget() const
{
	return m_MainBox;
}

void PlotterPanel::ParameterPanel::Update()
{
	m_ParameterBox->RemoveAll();
	for ( auto& x: m_Parameters ) {
		for ( auto& y: x.second ) {
			y.second->getModificationWidget()->Show(false);
		}
	}
	for ( auto& x : m_Parameters[m_CurrentPlotterName] ) {
		auto frame = sfg::Frame::Create();
		auto box = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
		frame->Add(box);
		box->Pack ( sfg::Label::Create ( x.first ), false, false );
		box->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );
		box->Pack ( x.second->getModificationWidget() );
		m_ParameterBox->Pack ( frame );
		x.second->getModificationWidget()->Show(true);
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//NEW DIFFERENTIAL WINDOW
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
PlotterPanel::NewDifferentialWindow::NewDifferentialWindow
(
	sfg::Desktop& desktop,
	std::function<void(FunctionInformation functionInformation)> callback
) :
	m_Desktop ( desktop ),
	m_Callback ( callback ),
	m_HasLowerBound ( false ),
	m_HasUpperBound ( false ),
	m_HasMinimumOut ( false ),
	m_HasMaximumOut ( false ),
	m_Window ( sfg::Window::Create() ),
	m_NameSelector ( sfg::ComboBox::Create() ),
	m_TypeSelector ( sfg::ComboBox::Create() ),
	m_OrientationSelector ( sfg::ComboBox::Create() ),
	m_LowerBoundToggle ( sfg::Button::Create ( "Add" ) ),
	m_UpperBoundToggle ( sfg::Button::Create ( "Add" ) ),
	m_LowerBoundEntry ( sfg::Entry::Create ( "-100" ) ),
	m_UpperBoundEntry ( sfg::Entry::Create ( "100" ) ),
	m_MinimumOutToggle ( sfg::Button::Create ( "Add" ) ),
	m_MaximumOutToggle ( sfg::Button::Create ( "Add" ) ),
	m_MinimumEntry ( sfg::Entry::Create ( "-1000" ) ),
	m_MaximumEntry ( sfg::Entry::Create ( "1000" ) ),
	m_PointGapToggle ( sfg::Button::Create ( "Custom" ) ),
	m_PointGapEntry ( sfg::Entry::Create ( "0.02" ) ),
	m_ColourSelector ( sfg::ComboBox::Create() ),
	m_RedEntry ( sfg::Entry::Create ( "255" ) ),
	m_GreenEntry ( sfg::Entry::Create ( "255" ) ),
	m_BlueEntry ( sfg::Entry::Create ( "255" ) ),
	m_CancelButton ( sfg::Button::Create ( "Cancel" ) ),
	m_DoneButton ( sfg::Button::Create ( "Done" ) )
{
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//LAYOUTING
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	auto box = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
	box->Pack ( sfg::Label::Create ( "Register a new differential" ), false, false );
	box->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );

	auto nameBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	nameBox->Pack ( sfg::Label::Create ( "Function" ), false, false );
	nameBox->Pack ( m_NameSelector, true, true );
	box->Pack ( nameBox, false, true );
	box->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );

	auto typeBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	typeBox->Pack ( sfg::Label::Create ( "Type" ), false, false );
	typeBox->Pack ( m_TypeSelector, true, true );
	typeBox->Pack ( m_OrientationSelector, true, true );
	box->Pack ( typeBox, false, true );
	box->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );

	auto lowerBoundBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	lowerBoundBox->Pack ( sfg::Label::Create ( "Lower bound" ), false, false );
	lowerBoundBox->Pack ( m_LowerBoundToggle, true, true );
	box->Pack ( lowerBoundBox, false, true );
	box->Pack ( m_LowerBoundEntry, false, true );

	auto upperBoundBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	upperBoundBox->Pack ( sfg::Label::Create ( "Upper bound" ), false, false );
	upperBoundBox->Pack ( m_UpperBoundToggle, true, true );
	box->Pack ( upperBoundBox, false, true );
	box->Pack ( m_UpperBoundEntry, false, true );

	auto minimumBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	minimumBox->Pack ( sfg::Label::Create ( "Minimum output" ), false, false );
	minimumBox->Pack ( m_MinimumOutToggle, true, true );
	box->Pack ( minimumBox, false, true );
	box->Pack ( m_MinimumEntry, false, true );

	auto maximumBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	maximumBox->Pack ( sfg::Label::Create ( "Maximum output" ), false, false );
	maximumBox->Pack ( m_MaximumOutToggle, true, true );
	box->Pack ( maximumBox, false, true );
	box->Pack ( m_MaximumEntry, false, true );

	auto pointGapBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	pointGapBox->Pack ( sfg::Label::Create ( "Point gap" ), false, false );
	pointGapBox->Pack ( m_PointGapToggle, true, true );
	box->Pack ( pointGapBox, false, true );
	box->Pack ( m_PointGapEntry, false, true );
	box->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );

	auto colourSelectorBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	colourSelectorBox->Pack ( sfg::Label::Create ( "Colour" ), false, false );
	colourSelectorBox->Pack ( m_ColourSelector, true, true );
	box->Pack ( colourSelectorBox, false, true );

	auto redBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	redBox->Pack ( sfg::Label::Create ( "Red" ), false, false );
	redBox->Pack ( m_RedEntry, true, true );
	box->Pack ( redBox, false, true );

	auto greenBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	greenBox->Pack ( sfg::Label::Create ( "Green" ), false, false );
	greenBox->Pack ( m_GreenEntry, true, true );
	box->Pack ( greenBox, false, true );

	auto blueBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	blueBox->Pack ( sfg::Label::Create ( "Blue" ), false, false );
	blueBox->Pack ( m_BlueEntry, true, true );
	box->Pack ( blueBox, false, true );
	box->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );

	auto buttonBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	buttonBox->Pack ( m_CancelButton, true, true );
	buttonBox->Pack ( m_DoneButton, true, true );
	box->Pack ( buttonBox, false, true );

	m_Window->Add ( box );
	m_Desktop.Add ( m_Window );
	m_Window->Show ( false );
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//SET UP
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	m_TypeSelector->AppendItem ( "Cartesian" );
	m_TypeSelector->AppendItem ( "Polar" );
	m_TypeSelector->SelectItem ( 0 );

	m_OrientationSelector->AppendItem ( "Horizontal -> Vertical" );
	m_OrientationSelector->AppendItem ( "Vertical -> Horizontal" );
	m_OrientationSelector->SelectItem ( 0 );

	m_LowerBoundEntry->Show ( false );
	m_UpperBoundEntry->Show ( false );
	Validator::ValidateEntry ( m_LowerBoundEntry, Validator::Trait::Decimal );
	Validator::ValidateEntry ( m_UpperBoundEntry, Validator::Trait::Decimal );

	m_MinimumEntry->Show ( false );
	m_MaximumEntry->Show ( false );
	Validator::ValidateEntry ( m_MinimumEntry, Validator::Trait::Decimal );
	Validator::ValidateEntry ( m_MaximumEntry, Validator::Trait::Decimal );

	m_PointGapEntry->Show ( false );
	Validator::ValidateEntry ( m_PointGapEntry, { Validator::Trait::Decimal, Validator::Trait::Positive, Validator::Trait::NonZero } );

	m_ColourSelector->AppendItem ( "White" );
	m_ColourSelector->AppendItem ( "Red" );
	m_ColourSelector->AppendItem ( "Orange" );
	m_ColourSelector->AppendItem ( "Yellow" );
	m_ColourSelector->AppendItem ( "Green" );
	m_ColourSelector->AppendItem ( "Cyan" );
	m_ColourSelector->AppendItem ( "Blue" );
	m_ColourSelector->AppendItem ( "Purple" );
	m_ColourSelector->AppendItem ( "Pink" );
	m_ColourSelector->AppendItem ( "Custom" );
	m_ColourSelector->SelectItem ( 0 );

	redBox->Show ( false );
	greenBox->Show ( false );
	blueBox->Show ( false );

	m_Window->SetAllocation ( { 0.f, 0.f, 0.f, 0.f } );

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//SIGNALS
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	m_TypeSelector->GetSignal ( sfg::ComboBox::OnSelect ).Connect ( [ this ] () {
		m_OrientationSelector->Clear();
		if ( m_TypeSelector->GetSelectedItem() == 0 ) { //Cartesian
			m_OrientationSelector->AppendItem ( "Horizontal -> Vertical" );
			m_OrientationSelector->AppendItem ( "Vertical -> Horizontal" );
			m_OrientationSelector->SelectItem ( 0 );
		} else if ( m_TypeSelector->GetSelectedItem() == 1 ) { //Polar
			m_OrientationSelector->AppendItem ( "Angular -> Radial" );
			m_OrientationSelector->AppendItem ( "Radial -> Angular" );
			m_OrientationSelector->SelectItem ( 0 );
		}
	} );

	m_LowerBoundToggle->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] () {
		if ( m_HasLowerBound ) {
			m_LowerBoundToggle->SetLabel ( "Add" );
			m_LowerBoundEntry->Show ( false );
			m_HasLowerBound = false;
		} else {
			m_LowerBoundToggle->SetLabel ( "Remove" );
			m_LowerBoundEntry->Show ( true );
			m_HasLowerBound = true;
		}
		m_Window->SetAllocation ( { 0.f, 0.f, 0.f, 0.f } );
	} );

	m_UpperBoundToggle->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] () {
		if ( m_HasUpperBound ) {
			m_UpperBoundToggle->SetLabel ( "Add" );
			m_UpperBoundEntry->Show ( false );
			m_HasUpperBound = false;
		} else {
			m_UpperBoundToggle->SetLabel ( "Remove" );
			m_UpperBoundEntry->Show ( true );
			m_HasUpperBound = true;
		}
		m_Window->SetAllocation ( { 0.f, 0.f, 0.f, 0.f } );
	} );

	m_MinimumOutToggle->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] () {
		if ( m_HasMinimumOut ) {
			m_MinimumOutToggle->SetLabel ( "Add" );
			m_MinimumEntry->Show ( false );
			m_HasMinimumOut = false;
		} else {
			m_MinimumOutToggle->SetLabel ( "Remove" );
			m_MinimumEntry->Show ( true );
			m_HasMinimumOut = true;
		}
		m_Window->SetAllocation ( { 0.f, 0.f, 0.f, 0.f } );
	} );

	m_MaximumOutToggle->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] () {
		if ( m_HasMaximumOut ) {
			m_MaximumOutToggle->SetLabel ( "Add" );
			m_MaximumEntry->Show ( false );
			m_HasMaximumOut = false;
		} else {
			m_MaximumOutToggle->SetLabel ( "Remove" );
			m_MaximumEntry->Show ( true );
			m_HasMaximumOut = true;
		}
		m_Window->SetAllocation ( { 0.f, 0.f, 0.f, 0.f } );
	} );

	m_PointGapToggle->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] () {
		if ( m_PointGapToggle->GetLabel() == "Custom" ) {
			m_PointGapToggle->SetLabel ( "Default" );
			m_PointGapEntry->Show ( true );
		} else {
			m_PointGapToggle->SetLabel ( "Custom" );
			m_PointGapEntry->Show ( false );
			m_PointGapEntry->SetText ( "0.02" );
		}
		m_Window->SetAllocation ( { 0.f, 0.f, 0.f, 0.f } );
	} );

	m_ColourSelector->GetSignal ( sfg::ComboBox::OnSelect ).Connect ( [ this, redBox, greenBox, blueBox ] () {
		switch ( m_ColourSelector->GetSelectedItem() ) {
		case 0: //White
			m_RedEntry->SetText ( "255" );
			m_GreenEntry->SetText ( "255" );
			m_BlueEntry->SetText ( "255" );

			redBox->Show ( false );
			greenBox->Show ( false );
			blueBox->Show ( false );
			break;

		case 1: //Red
			m_RedEntry->SetText ( "255" );
			m_GreenEntry->SetText ( "0" );
			m_BlueEntry->SetText ( "0" );

			redBox->Show ( false );
			greenBox->Show ( false );
			blueBox->Show ( false );
			break;

		case 2: //Orange
			m_RedEntry->SetText ( "255" );
			m_GreenEntry->SetText ( "128" );
			m_BlueEntry->SetText ( "0" );

			redBox->Show ( false );
			greenBox->Show ( false );
			blueBox->Show ( false );
			break;

		case 3: //Yellow
			m_RedEntry->SetText ( "255" );
			m_GreenEntry->SetText ( "255" );
			m_BlueEntry->SetText ( "0" );

			redBox->Show ( false );
			greenBox->Show ( false );
			blueBox->Show ( false );
			break;

		case 4: //Green
			m_RedEntry->SetText ( "0" );
			m_GreenEntry->SetText ( "255" );
			m_BlueEntry->SetText ( "0" );

			redBox->Show ( false );
			greenBox->Show ( false );
			blueBox->Show ( false );
			break;

		case 5: //Cyan
			m_RedEntry->SetText ( "0" );
			m_GreenEntry->SetText ( "255" );
			m_BlueEntry->SetText ( "255" );

			redBox->Show ( false );
			greenBox->Show ( false );
			blueBox->Show ( false );
			break;

		case 6: //Blue
			m_RedEntry->SetText ( "0" );
			m_GreenEntry->SetText ( "0" );
			m_BlueEntry->SetText ( "255" );

			redBox->Show ( false );
			greenBox->Show ( false );
			blueBox->Show ( false );
			break;

		case 7: //Purple
			m_RedEntry->SetText ( "128" );
			m_GreenEntry->SetText ( "0" );
			m_BlueEntry->SetText ( "128" );

			redBox->Show ( false );
			greenBox->Show ( false );
			blueBox->Show ( false );
			break;

		case 8: //Pink
			m_RedEntry->SetText ( "255" );
			m_GreenEntry->SetText ( "0" );
			m_BlueEntry->SetText ( "255" );

			redBox->Show ( false );
			greenBox->Show ( false );
			blueBox->Show ( false );
			break;

		case 9: //Custom
			redBox->Show ( true );
			greenBox->Show ( true );
			blueBox->Show ( true );
			break;
		}
		m_Window->SetAllocation ( { 0.f, 0.f, 0.f, 0.f } );
	} );

	m_CancelButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] () {
		Hide();
	} );

	m_DoneButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] () {
		std::string lowerBound ( "0.0" );
		std::string upperBound ( "0.0" );
		std::string minimumOut ( "0.0" );
		std::string maximumOut ( "0.0" );
		if ( m_HasLowerBound ) {
			lowerBound = m_LowerBoundEntry->GetText();
		}
		if ( m_HasUpperBound ) {
			upperBound = m_UpperBoundEntry->GetText();
		}
		if ( m_HasMinimumOut ) {
			minimumOut = m_MinimumEntry->GetText();
		}
		if ( m_HasMaximumOut ) {
			maximumOut = m_MaximumEntry->GetText();
		}
		m_Callback (
			FunctionInformation {
				m_NameSelector->GetSelectedText(),
				"",
				static_cast<unsigned> (m_TypeSelector->GetSelectedItem()), //Although GetSelectedItem returns an int, it has been
				static_cast<unsigned> (m_OrientationSelector->GetSelectedItem()), //set up so that it can never be negative.
				m_HasLowerBound,
				m_HasUpperBound,
				std::stod ( lowerBound ),
				std::stod ( upperBound ),
				m_HasMinimumOut,
				m_HasMaximumOut,
				std::stod ( minimumOut ),
				std::stod ( maximumOut ),
				std::stod ( std::string (m_PointGapEntry->GetText() ) ),
				sf::Color (
					std::stoul ( std::string ( m_RedEntry->GetText() ) ),
					std::stoul ( std::string ( m_GreenEntry->GetText() ) ),
					std::stoul ( std::string ( m_BlueEntry->GetText() ) )
				)
			}
		);
		Hide();
	} );
}

void PlotterPanel::NewDifferentialWindow::Show ( std::set<std::string> functionNames )
{
	m_NameSelector->Clear();
	for ( auto& x: functionNames ) {
		m_NameSelector->AppendItem ( x );
	}
	m_NameSelector->SelectItem ( 0 );
	m_Window->Show ( true );
	m_Desktop.BringToFront ( m_Window );
}

void PlotterPanel::NewDifferentialWindow::Hide()
{
	m_Window->Show ( false );
}

}
