#ifndef PARAMETER_HPP_INCLUDED
#define PARAMETER_HPP_INCLUDED

//STANDARD LIBRARY INCLUDES
#include <functional> // std::function
#include <vector>     // std::vector

//SFGUI INCLUDES
#include <SFGUI/ComboBox.hpp>    // sfg::ComboBox
#include <SFGUI/Entry.hpp>       // sfg::Entry
#include <SFGUI/Label.hpp>       // sfg::Label
#include <SFGUI/ProgressBar.hpp> // sfg::ProgressBar
#include <SFGUI/Scale.hpp>       // sfg::Scale
#include <SFGUI/Widget.hpp>      // sfg::Widget

//INTERNAL INCLUDES
#include "delayedFunction.hpp" // ui::DelayedEvent, ui::DelayedEventQueue

namespace ui {

////////////////////////////////////////////////////////////////////////////////
/// \brief Abstract base class for all types of Parameter.
///
/// Parameters are used when variables in functions that are to be plotted on
/// screen need to be given a value. They control that one value is a way that
/// is dependent on their type. Because Parameter is an abstract base class, it
/// cannot be used without being subclassed, which makes sense because a
/// Parameter has to control its variable in a specific way dependent on type.
////////////////////////////////////////////////////////////////////////////////
class Parameter {

public:

	////////////////////////////////////////////////////////////////////////////
	/// \brief Get the numeric value of the Parameter.
	///
	/// As parameters control the value of variables, you have to be able to
	/// actually get their value. This also allows for the value to be checked
	/// once and stored before the plotting begins. If this could not be done,
	/// then animated graphs would change over their length as plotting takes
	/// time and goes from one side to the other.
	////////////////////////////////////////////////////////////////////////////
	virtual double getValue() const = 0;

	////////////////////////////////////////////////////////////////////////////
	/// \brief Returns a widget that can be used to modify the Parameter.
	///
	/// The modification widget is so set up that it allows the user to alter
	/// properties of that parameter. It is ( at the moment ) always a container
	/// widget which holds Entries and Buttons, however it can be anything that
	/// does its job, which is why a generic Widget::Ptr is returned. All types
	/// of parameter have a modification widget.
	////////////////////////////////////////////////////////////////////////////
	virtual sfg::Widget::Ptr getModificationWidget() const = 0;

	////////////////////////////////////////////////////////////////////////////
	/// \brief Returns a widget that acts as a control for this Parameter.
	///
	/// The user widget should be overlayed on top the graph so that the user
	/// can dynamically alter the value of the Parameter. Some types of
	/// parameter may not have a user widget, because it does not make sense for
	/// the user to modify their value in that way. In that case, the value will
	/// be able to be modified from the modification widget.
	////////////////////////////////////////////////////////////////////////////
	virtual sfg::Widget::Ptr getUserWidget() const = 0;

	////////////////////////////////////////////////////////////////////////////
	/// \brief Saves the state of the parameter so that it can be recreated.
	///
	/// This is used internally when the save button is pressed in order to get
	/// some strings from which existing parameters could be recreated.
	////////////////////////////////////////////////////////////////////////////
	virtual void save( std::queue<std::string>& saveInformation ) const =0;

	////////////////////////////////////////////////////////////////////////////
	/// \brief Virtual destrcutor.
	///
	/// The destructor must be declared virtual because this is a polymorphic
	/// base class, so derived classes may well be stored simply by a pointer (
	/// hopefully a smart pointer ) to Parameter. As such, if the destructor was
	/// not virtual, Parameter's destructor would be called, rather than the
	/// derived class' desctructor, which could lead to memory leaks.
	////////////////////////////////////////////////////////////////////////////
	virtual ~Parameter() = default;

	////////////////////////////////////////////////////////////////////////////
	/// \brief Create a parameter from save information
	///
	/// This function actually just delegates to the appropriate dervied class
	/// loading function.
	////////////////////////////////////////////////////////////////////////////
	static std::unique_ptr<Parameter> load ( std::queue<std::string>& saveInformation, DelayedEventQueue& delayedEventQueue, std::vector<std::weak_ptr<std::function<void ( float ) >>>& updateQueue );
};


////////////////////////////////////////////////////////////////////////////////
/// \brief A Parameter whose value is controlled by an on screen slider.
///
/// SliderParameter is probably the go to parameter for most variables. As such,
/// it is the default parameter in the ActivationWindow. It allows the user to
/// use a slider to adjust the value contained within the Parameter and also
/// allows the user to modify the slider so that it is set up for whatever range
/// or precision the user wants.
////////////////////////////////////////////////////////////////////////////////
class SliderParameter: public Parameter {

public:

	////////////////////////////////////////////////////////////////////////////
	/// \brief Constructor.
	///
	/// This creates and sets up the SliderParameter. Its default lower bound
	/// is 0.0, its upper bound is 1.0, it takes 20 steps to get there, and its
	/// initial value is 0.
	///
	/// \param delayedEventQueue This is required in order to set up the
	/// Parameter's DelayedEvents. It has these because of Widgets accessing
	/// other Widgets when properties are updated.
	////////////////////////////////////////////////////////////////////////////
	SliderParameter (
		DelayedEventQueue& delayedEventQueue
	);

	////////////////////////////////////////////////////////////////////////////
	/// \brief Returns the value of the Parameter.
	///
	/// For a SliderParameter, this value is calculated by finding what
	/// proportion of the distance along the slider the handle is, and finding
	/// and finding the value that is that proportion from the lower bound to
	/// the upper bound.
	////////////////////////////////////////////////////////////////////////////
	virtual double getValue() const override;

	////////////////////////////////////////////////////////////////////////////
	/// \brief Returns the modification widget.
	///
	/// The modification widget allows the user to alter the lower bound, upper
	/// bound and number of steps it takes to get from one to the other. Changed
	/// values must be confirmed via the set button.
	////////////////////////////////////////////////////////////////////////////
	virtual sfg::Widget::Ptr getModificationWidget() const override;

	////////////////////////////////////////////////////////////////////////////
	/// \brief Returns the user widget.
	///
	/// The user widget contains the slider that the user can use to change the
	/// value of this Parameter. It also contains a text readout of the value of
	/// the Parameter.
	////////////////////////////////////////////////////////////////////////////
	virtual sfg::Widget::Ptr getUserWidget() const override;

	////////////////////////////////////////////////////////////////////////////
	/// \brief Saves the state of the parameter so that it can be recreated.
	///
	/// This is used internally when the save button is pressed in order to get
	/// some strings from which existing parameters could be recreated.
	////////////////////////////////////////////////////////////////////////////
	virtual void save( std::queue<std::string>& saveInformation ) const;

	////////////////////////////////////////////////////////////////////////////
	/// \brief Destructor.
	///
	/// Default destructor.
	////////////////////////////////////////////////////////////////////////////
	~SliderParameter() = default;

	////////////////////////////////////////////////////////////////////////////
	/// \brief Create a slider parameter from save information
	///
	/// This should only really be called from Parameter::load()
	////////////////////////////////////////////////////////////////////////////
	static std::unique_ptr<Parameter> load ( std::queue<std::string>& saveInformation, DelayedEventQueue& delayedEventQueue );

private:

	sfg::Widget::Ptr m_UserWidget;
	sfg::Widget::Ptr m_ModificationWidget;

	sfg::Label::Ptr m_Label;
	sfg::Scale::Ptr m_Slider;

	sfg::Entry::Ptr m_LowerEntry;
	sfg::Entry::Ptr m_UpperEntry;
	sfg::Entry::Ptr m_StepsEntry;

	struct Save {
		std::string lower;
		std::string upper;
		std::string steps;
		float value;
	};
	Save m_Save;

	DelayedEvent<void> UpdateUserLabel;
	DelayedEvent<void> UpdateUserSlider;
};

////////////////////////////////////////////////////////////////////////////////
/// \brief A parameter with a set value.
///
/// Constant parameters simply represent a number, nothing else. The default is
/// 0.0, and the value can be changed by the user widget. There is no
/// modification widget.
////////////////////////////////////////////////////////////////////////////////
class ConstantParameter: public Parameter {

public:

	////////////////////////////////////////////////////////////////////////////
	/// \brief Constructor.
	///
	/// Creates and sets up the ConstantParameter with a value of 0.0
	///
	/// \param delayedEventQueue This is needed in order to set up this
	/// Parameter's DelayedEvent, which is required because of accessing an
	/// Entry Widget when the value is updated.
	////////////////////////////////////////////////////////////////////////////
	ConstantParameter (
		DelayedEventQueue& delayedEventQueue
	);

	////////////////////////////////////////////////////////////////////////////
	/// \brief Returns the value of this Parameter.
	///
	/// The value is simply a text to number conversion on the contents of the
	/// modification widget entry, whenever the set button is pressed. It is
	/// validated by Validator::ValidateEntry, as being a signed decimal, so any
	/// number is valid.
	////////////////////////////////////////////////////////////////////////////
	virtual double getValue() const override;

	////////////////////////////////////////////////////////////////////////////
	/// \brief Returns the modification widget.
	///
	/// The modification widget is simply a text entry for a new value to be
	/// typed into, and a button to make this new value take effect.
	////////////////////////////////////////////////////////////////////////////
	virtual sfg::Widget::Ptr getModificationWidget() const override;

	////////////////////////////////////////////////////////////////////////////
	/// \brief Returns the user widget (empty).
	///
	/// ConstantParameters do not have a user widget, because their value is not
	/// meant to be dynamic. It can be changed by the modification widget
	/// though.
	////////////////////////////////////////////////////////////////////////////
	virtual sfg::Widget::Ptr getUserWidget() const override;

	////////////////////////////////////////////////////////////////////////////
	/// \brief Saves the state of the parameter so that it can be recreated.
	///
	/// This is used internally when the save button is pressed in order to get
	/// some strings from which existing parameters could be recreated.
	////////////////////////////////////////////////////////////////////////////
	virtual void save( std::queue<std::string>& saveInformation ) const;

	////////////////////////////////////////////////////////////////////////////
	/// \brief Destructor.
	///
	/// Default destructor.
	////////////////////////////////////////////////////////////////////////////
	~ConstantParameter() = default;

	////////////////////////////////////////////////////////////////////////////
	/// \brief Create a constant parameter from save information
	///
	/// This should only really be called from Parameter::load()
	////////////////////////////////////////////////////////////////////////////
	static std::unique_ptr<Parameter> load ( std::queue<std::string>& saveInformation, DelayedEventQueue& delayedEventQueue );

private:

	sfg::Widget::Ptr m_UserWidget;
	sfg::Widget::Ptr m_ModificationWidget;

	sfg::Entry::Ptr m_ValueEntry;

	double m_Value;

	std::string m_SavedText;

	DelayedEvent<void> UpdateValue;

};

////////////////////////////////////////////////////////////////////////////////
/// \brief A Parameter whose value changes over time.
///
/// TimeParameters can be used to animate graphs, as their value changes at a
/// fixed rate over time. The default value is 0.0, default lower bound is 0.0,
/// default upper bound is 1.0, default time period is 1.0s and default mode is
/// to loop, rather than oscillate.
////////////////////////////////////////////////////////////////////////////////
class TimeParameter: public Parameter {

public:

	////////////////////////////////////////////////////////////////////////////
	/// \brief Constructor.
	///
	/// This constructs a TimeParameter with all default values. It starts off
	/// playing, but can be paused from the user widget.
	///
	/// \param delayedEventQueue Allows the setting up of TimeParameter's
	/// DelayedEvents.
	/// \param updateQueue This is needed so that TimeParameter is modified
	/// based on how long it was since the last frame. Allowing it to move at a
	/// frame rate independent speed.
	////////////////////////////////////////////////////////////////////////////
	TimeParameter (
		DelayedEventQueue&                                          delayedEventQueue,
		std::vector<std::weak_ptr<std::function<void ( float ) >>>& updateQueue
	);

	////////////////////////////////////////////////////////////////////////////
	/// \brief Destructor.
	///
	/// Default destructor.
	////////////////////////////////////////////////////////////////////////////
	~TimeParameter() = default;

	////////////////////////////////////////////////////////////////////////////
	/// \brief Returns the current value of the TimeParameter.
	///
	/// This function is especially useful for TimeParameter, because if it
	/// simply modified a value in real time, then different parts of a graph
	/// would get different values from its variable. e.g. a graph of y=ax where
	/// a is a TimeParameter, would bend rather than being a straight line.
	/// Because the value of the TimeParameter is a double, but the proportion
	/// of a ProgressBar is a float, the value is directly modified based on
	/// time, and then the ProgressBar is modified to reflect that, not the
	/// other way around.
	////////////////////////////////////////////////////////////////////////////
	virtual double getValue() const override;

	////////////////////////////////////////////////////////////////////////////
	/// \brief Returns the modification widget.
	///
	/// TimeParameter's modification widget allows the user to change the lower
	/// bound, the upper bound, the time period and the mode ( loop or oscillate
	/// ).
	////////////////////////////////////////////////////////////////////////////
	virtual sfg::Widget::Ptr getModificationWidget() const override;

	////////////////////////////////////////////////////////////////////////////
	/// \brief Returns the user widget.
	///
	/// The user widget shows the TimeParameter's value via a ProgressBar, as
	/// well as a Label that displays the value as text. It also allows the user
	/// the play or pause the TimeParameter.
	////////////////////////////////////////////////////////////////////////////
	virtual sfg::Widget::Ptr getUserWidget() const override;

	////////////////////////////////////////////////////////////////////////////
	/// \brief The different modes with which a TimeParameter can play.
	///
	/// These modes control what happens when a TimeParameter reaches its upper
	/// bound. It can eitehr immediately go back to its lower bound and increase
	/// again, or it can start decreasing.
	////////////////////////////////////////////////////////////////////////////
	enum class Mode {
		loop, ///< Value resets to lower bound when it hits upper bound.
		oscillate ///< Value starts decreasing when it hits upper bound.
	};

	////////////////////////////////////////////////////////////////////////////
	/// \brief Set the play mode.
	///
	/// This is normally done by the modification widget, but can be done
	/// programmatically as well. The different modes are explained in the
	/// documentation of Mode.
	////////////////////////////////////////////////////////////////////////////
	void setMode (
		Mode mode
	);

	////////////////////////////////////////////////////////////////////////////
	/// \brief Saves the state of the parameter so that it can be recreated.
	///
	/// This is used internally when the save button is pressed in order to get
	/// some strings from which existing parameters could be recreated.
	////////////////////////////////////////////////////////////////////////////
	virtual void save( std::queue<std::string>& saveInformation ) const;

	////////////////////////////////////////////////////////////////////////////
	/// \brief Create a time parameter from save information
	///
	/// This should only really be called from Parameter::load()
	////////////////////////////////////////////////////////////////////////////
	static std::unique_ptr<Parameter> load ( std::queue<std::string>& saveInformation, DelayedEventQueue& delayedEventQueue, std::vector<std::weak_ptr<std::function<void ( float ) >>>& updateQueue );

private:

	sfg::Widget::Ptr m_UserWidget;
	sfg::Widget::Ptr m_ModificationWidget;

	Mode m_Mode;

	bool m_Playing;
	double m_Start;
	double m_End;
	double m_TimePeriod;
	double m_Value;

	sfg::Label::Ptr m_ValueLabel;
	sfg::ProgressBar::Ptr m_Bar;

	sfg::Entry::Ptr m_StartEntry;
	sfg::Entry::Ptr m_EndEntry;
	sfg::Entry::Ptr m_TimeEntry;
	sfg::ComboBox::Ptr m_ModeComboBox;

	struct Save {
		std::string start;
		std::string end;
		std::string time;
		int mode;
	};
	Save m_Save;

	std::shared_ptr<std::function<void(float)>> m_UpdateFunction;

	DelayedEvent<void> UpdateUserWidget;
	DelayedEvent<void> UpdateValues;
};

} //END NAMESPACE ui

#endif // PARAMETER_HPP_INCLUDED
