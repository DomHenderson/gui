//STANDARD LIBRARY INCLUDES
#include <algorithm>        // std::transform
#include <initializer_list> // std::initializer_list
#include <functional>       // std::function
#include <regex>            // std::regex, std::regex_match
#include <string>           // std::string
#include <unordered_map>    // std::unordered_map
#include <vector>           // std::vector

//SFGUI INCLUDES
#include <SFGUI/Entry.hpp> // sfg::Entry

//INTERNAL INCLUDES
#include "validator.hpp" // ui::Validator::Trait, ui::Validator::ValidateEntry

//This is a namespace not a class, because there should not be a need to
//initialise it, nor pass it around.
namespace ui::Validator {

namespace { //anonymous

	//Maps the Traits to a regex that can be used to check for their presence
	const std::unordered_map<Trait, std::regex> regexes ( {
		//One or more letters
		{ Trait::Alphabetic, std::regex ( R"([a-zA-Z]+)" ) },

		//An optional sign character followed by one or more digits, and an optional point with one or more digits after it
		{ Trait::Decimal, std::regex ( R"([\+\-]?[0-9]+(\.[0-9]+)?)" ) },

		//An optional sign charater followed by one or more digits and nothing else
		{ Trait::Integer, std::regex ( R"([\+\-]?[0-9]+)" ) },

		//Anything that starts with a -
		{ Trait::Negative, std::regex ( R"(\-.*)" ) },

		//Zero is defined as an optional sign charater followed by one or more 0s, with an optional point and more zeros until the end of the string.
		//This uses a negative lookahead to match anything other than that.
		{ Trait::NonZero, std::regex ( R"((?![\+\-]?0+\.?0*$).*)" ) },

		//A positive integer followed by a % sign
		{ Trait::Percentage, std::regex ( R"(\+?[0-9]+%)" ) },

		//Anything that doesn't start with a -
		//This should be used in conjunction with Decimal or Integer if a positive number is desired.
		{ Trait::Positive, std::regex ( R"([^\-].*)" ) }
	} );

	//This overload is designed to avoid having to put curly braces around a
	//single Trait. Hence it's really here for convenience.
	bool HasTraits (
		std::string text,
		Trait trait
	)
	{
		return std::regex_match ( text, regexes.at ( trait ) );
	}

	//A useful overload that can be replicated by using the overload which takes
	//a function, but it is more convenient to simply be able to check against
	//more than one Trait.
	bool HasTraits (
		std::string text,
		std::initializer_list<Trait> traits
	)
	{
		bool result ( true );

		for ( auto& x : traits ) {
			result = result && std::regex_match ( text, regexes.at ( x ) );
		}

		return result;
	}

	//The most compilcated overload. This gives full control to the user of the
	//function, however it is more verbose and most likely less efficient as
	//well. Compilers are very good at optimisation though, so it's unlikely to
	//make a difference.
	//The function passed to this function is for determining whether the Traits
	//that match the string are sufficient for it to be valid
	bool HasTraits (
		std::string text,
		std::initializer_list<Trait> traits,
		std::function<bool ( std::vector<bool> ) > function
	)
	{
		//The passed evaluation function takes a vector of bools, so the results
		//must be stored in a vector of bools. The bool specialisation of vector
		//is not perfect, but it shouldn't be a problem here due to the small
		//size of this vector.
		std::vector<bool> results;

		//Dynamic resizing of vectors is inefficient, so reserve is used to pre-
		//allocate, but not initialise or spent time doing anything with, enough
		//memory that the vector won't need to change size
		results.reserve ( traits.size() );

		//Transform takes a range (in this case traits), and uses a function to
		//map it onto another range (in this case results).
		std::transform (
			traits.begin(),
			traits.end(),
			results.begin(),
			[ text ] ( Trait trait ) -> bool { //comparison function
				return std::regex_match ( text, regexes.at ( trait ) );
			}
		);

		//The user defined function determines whether the combination of traits
		//that are actually present in the text is valid, so return its output.
		return function ( results );
	}

} //END NAMESPACE ui::Validator::{anonymous}

//Each overload of ValidateEntry corresponds to an overload of HasTraits
void ValidateEntry (
	sfg::Entry::Ptr entry,
	Trait trait
)
{
	//A weak ptr is used here to avoid a circular dependency.
	//Before this fix, validated entries could not be destroyed until the end of the program,
	//because they indirectly owned themselves.
	//Weak_ptrs do not own objects, but allow you to easily check for their existance,
	//and then create a shared_ptr if they do exist so that they continue to exist while you use them.
	std::weak_ptr<sfg::Entry> entryWeakPtr = entry;

	//Connect a validating lambda function to the entry such that it fires when
	//the text in the entry changes
	entry->GetSignal ( sfg::Entry::OnTextChanged ).Connect (
		[ entryWeakPtr, trait ] {
			if ( auto entrySharedPtr = entryWeakPtr.lock() ) {
				if ( entrySharedPtr->GetId ( ) == "Invalid" ) {
					if ( HasTraits ( entrySharedPtr->GetText(), trait ) ) {
						entrySharedPtr->SetId ( "Valid" );
					}
				} else { //entry is currently valid
					if ( !HasTraits ( entrySharedPtr->GetText(), trait ) ) {
						entrySharedPtr->SetId ( "Invalid" );
					}
				}
			}
		}
	);
}

//Corresponds to the overload of HasTraits that takes multiple Traits but no
//function.
void ValidateEntry (
	sfg::Entry::Ptr entry,
	std::initializer_list<Trait> traits
)
{
	//A weak ptr is used here to avoid a circular dependency.
	//Before this fix, validated entries could not be destroyed until the end of the program,
	//because they indirectly owned themselves.
	//Weak_ptrs do not own objects, but allow you to easily check for their existance,
	//and then create a shared_ptr if they do exist so that they continue to exist while you use them.
	std::weak_ptr<sfg::Entry> entryWeakPtr = entry;

	//Connect a validating lambda function to the entry such that it fires when
	//the text in the entry changes
	entry->GetSignal ( sfg::Entry::OnTextChanged ).Connect (
		[ entryWeakPtr, traits ] {
			if ( auto entrySharedPtr = entryWeakPtr.lock() ) {
				if ( entrySharedPtr->GetId ( ) == "Invalid" ) {
					if ( HasTraits ( entrySharedPtr->GetText(), traits ) ) {
						entrySharedPtr->SetId ( "Valid" );
					}
				} else { //entry is currently valid
					if ( !HasTraits ( entrySharedPtr->GetText(), traits ) ) {
						entrySharedPtr->SetId ( "Invalid" );
					}
				}
			}
		}
	);
}

//Corresponds to the overload of HasTraits that uses a function to check whether
//the combination of present Traits is valid
void ValidateEntry (
	sfg::Entry::Ptr entry,
	std::initializer_list<Trait> traits,
	std::function<bool ( std::vector<bool> ) > function
)
{
	//A weak ptr is used here to avoid a circular dependency.
	//Before this fix, validated entries could not be destroyed until the end of the program,
	//because they indirectly owned themselves.
	//Weak_ptrs do not own objects, but allow you to easily check for their existance,
	//and then create a shared_ptr if they do exist so that they continue to exist while you use them.
	std::weak_ptr<sfg::Entry> entryWeakPtr = entry;

	//Connect a validating lambda function to the entry such that it fires when
	//the text in the entry changes
	entry->GetSignal ( sfg::Entry::OnTextChanged ).Connect (
		[ entryWeakPtr, traits, function ] {
			if ( auto entrySharedPtr = entryWeakPtr.lock() ) {
				if ( entrySharedPtr->GetId ( ) == "Invalid" ) {
					if ( HasTraits ( entrySharedPtr->GetText(), traits, function ) ) {
						entrySharedPtr->SetId ( "Valid" );
					}
				} else { //entry is currently valid
					if ( !HasTraits ( entrySharedPtr->GetText(), traits, function ) ) {
						entrySharedPtr->SetId ( "Invalid" );
					}
				}
			}
		}
	);
}

} //END NAMESPACE ui
