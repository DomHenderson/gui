#ifndef PLOTTERPANEL_HPP_INCLUDED
#define PLOTTERPANEL_HPP_INCLUDED

//STANDARD LIBRARY INCLUDES
#include <functional> //std::function
#include <map>        //std::map
#include <memory>     //std::unique_ptr
#include <set>        //std::set
#include <string>     //std::string
#include <vector>     //std::vector

//SFML INCLUDES
#include <SFML/Graphics/Color.hpp> //sf::Color

//SFGUI INCLUDES
#include <SFGUI/Button.hpp>         //sfg::Button
#include <SFGUI/ComboBox.hpp>       //sfg::ComboBox
#include <SFGUI/Desktop.hpp>        //sfg::Desktop
#include <SFGUI/ScrolledWindow.hpp> //sfg::ScrolledWindow
#include <SFGUI/Widget.hpp>         //sfg::Widget

//EXPRESSION COMPILER 2 INCLUDES
#include <ExpressionCompiler2/compiler.hpp> //ec::Compiler

//GRAPH CANVAS INCLUDES
#include <GraphCanvas/graphCanvas.hpp> //gc::GraphCanvas

//INTERNAL INCLUDES
#include "parameter.hpp" //ui::Parameter

namespace ui {

class PlotterPanel {
public:
	PlotterPanel(
		sfg::Desktop& desktop,
		gc::GraphCanvas& graphCanvas,
		ec::Compiler& compiler,
		std::vector<std::weak_ptr<std::function<void(float)>>>& updateQueue,
		DelayedEventQueue& delayedEventQueue
	);

	void reset();
	void save ( std::queue<std::string>& saveInformation );
	void load ( std::queue<std::string>& saveInformation );

	sfg::Widget::Ptr getWidget() const;
	sfg::Widget::Ptr getParameterPanelWidget() const;
private:
	struct FunctionInformation {
		std::string name;
		std::string expression;
		unsigned type;
		unsigned orientation;
		bool hasLowerBound;
		bool hasUpperBound;
		double lowerBound;
		double upperBound;
		bool hasMinimumOut;
		bool hasMaximumOut;
		double minimumOut;
		double maximumOut;
		double pointGap;
		sf::Color colour;
	};

	void AddDifferential ( FunctionInformation functionInformation );
	void AddFunction ( FunctionInformation functionInformation );
	void AddParameter ( std::string name, std::unique_ptr<Parameter> parameter );
	void AddPlotter ( std::string name, std::string variable );
	void DeleteFunction ( std::string name );
	void DeletePlotter ( std::string name );
	void RecreateFunctionView ();
	void RecreateParameterView ();

	std::map<std::string, std::map<std::string, FunctionInformation>> m_FunctionSets;
	std::map<std::string, std::map<std::string,std::unique_ptr<Parameter>>> m_ParameterSets;
	std::string m_CurrentPlotterName;

	std::shared_ptr<std::function<void(float)>> m_UpdateFunction;

	ec::Compiler& m_Compiler;
	gc::GraphCanvas& m_GraphCanvas;
	DelayedEventQueue& m_DelayedEventQueue;
	std::vector<std::weak_ptr<std::function<void(float)>>>& m_UpdateQueue;

	//WIDGETS!!!!
	sfg::Widget::Ptr m_MainWidget;

	//Left panel
	sfg::ComboBox::Ptr m_PlotterSelector;
	sfg::Button::Ptr m_NewPlotterButton;
	sfg::Button::Ptr m_NewFunctionButton;
	sfg::Button::Ptr m_NewDifferentialButton;
	sfg::Button::Ptr m_DeleteFunctionButton;
	sfg::Button::Ptr m_DeletePlotterButton;

	//Middle panel
	sfg::ScrolledWindow::Ptr m_FunctionView;

	//Right panel
	sfg::ScrolledWindow::Ptr m_ParameterView;

	class NewPlotterWindow {
	public:
		NewPlotterWindow ( sfg::Desktop& desktop, std::function<void(std::string,std::string)> callback );
		void Show();
	private:
		void Hide();

		sfg::Desktop& m_Desktop;
		std::function<void(std::string, std::string)> m_Callback;

		sfg::Window::Ptr m_Window;
		sfg::Entry::Ptr m_NameEntry;
		sfg::Entry::Ptr m_VarEntry;
		sfg::Button::Ptr m_CancelButton;
		sfg::Button::Ptr m_DoneButton;
	};
	NewPlotterWindow m_NewPlotterWindow;

	class NewFunctionWindow {
	public:
		NewFunctionWindow (
			ec::Compiler& compiler,
			sfg::Desktop& desktop,
			std::function<void(FunctionInformation)> callback
		);
		void Show();
	private:
		void Hide();

		ec::Compiler& m_Compiler;
		sfg::Desktop& m_Desktop;
		std::function<void(FunctionInformation)> m_Callback;

		bool m_HasLowerBound;
		bool m_HasUpperBound;
		bool m_HasMinimumOut;
		bool m_HasMaximumOut;

		sfg::Window::Ptr m_Window;
		sfg::Entry::Ptr m_NameEntry;
		sfg::Entry::Ptr m_ExpressionEntry;
		sfg::ComboBox::Ptr m_TypeSelector;
		sfg::ComboBox::Ptr m_OrientationSelector;
		sfg::Button::Ptr m_LowerBoundToggle;
		sfg::Button::Ptr m_UpperBoundToggle;
		sfg::Entry::Ptr m_LowerBoundEntry;
		sfg::Entry::Ptr m_UpperBoundEntry;
		sfg::Button::Ptr m_MinimumOutToggle;
		sfg::Button::Ptr m_MaximumOutToggle;
		sfg::Entry::Ptr m_MinimumEntry;
		sfg::Entry::Ptr m_MaximumEntry;
		sfg::Button::Ptr m_PointGapToggle;
		sfg::Entry::Ptr m_PointGapEntry;
		sfg::ComboBox::Ptr m_ColourSelector;
		sfg::Entry::Ptr m_RedEntry;
		sfg::Entry::Ptr m_GreenEntry;
		sfg::Entry::Ptr m_BlueEntry;
		sfg::Button::Ptr m_CancelButton;
		sfg::Button::Ptr m_DoneButton;
	};
	NewFunctionWindow m_NewFunctionWindow;

	class LinkingWindow {
	public:
		LinkingWindow (
			sfg::Desktop& desktop,
			DelayedEventQueue& delayedEventQueue,
			std::vector<std::weak_ptr<std::function<void(float)>>>& updateQueue,
			std::function<void(std::string, std::unique_ptr<Parameter>)> callback
		);
		void Show( std::set<std::string> names );
	private:
		void Hide();

		sfg::Desktop& m_Desktop;
		DelayedEventQueue& m_DelayedEventQueue;
		std::vector<std::weak_ptr<std::function<void(float)>>>& m_UpdateQueue;
		std::function<void(std::string, std::unique_ptr<Parameter>)> m_Callback;

		std::map<std::string, unsigned> m_Parameters;

		sfg::Window::Ptr m_Window;
		sfg::Box::Ptr m_ParameterBox;
		sfg::Button::Ptr m_CancelButton;
		sfg::Button::Ptr m_DoneButton;
	};
	LinkingWindow m_LinkingWindow;

	class DeleteFunctionWindow {
	public:
		DeleteFunctionWindow (
			sfg::Desktop& desktop,
			std::function<void(std::string)> callback
		);
		void Show( std::set<std::string> functionNames );
	private:
		void Hide();

		sfg::Desktop& m_Desktop;
		std::function<void(std::string)> m_Callback;

		sfg::Window::Ptr m_Window;
		sfg::ComboBox::Ptr m_Selector;
		sfg::Button::Ptr m_CancelButton;
		sfg::Button::Ptr m_DoneButton;
	};
	DeleteFunctionWindow m_DeleteFunctionWindow;

	class DeletePlotterWindow {
	public:
		DeletePlotterWindow (
			sfg::Desktop& desktop,
			std::function<void(std::string)> callback
		);
		void Show ( std::set<std::string> plotterNames );
	private:
		void Hide();

		sfg::Desktop& m_Desktop;
		std::function<void(std::string)> m_Callback;

		sfg::Window::Ptr m_Window;
		sfg::ComboBox::Ptr m_Selector;
		sfg::Button::Ptr m_CancelButton;
		sfg::Button::Ptr m_DoneButton;
	};
	DeletePlotterWindow m_DeletePlotterWindow;

	class ParameterPanel {
	public:
		ParameterPanel (
			std::map<std::string, std::map<std::string, std::unique_ptr<Parameter>>>& parameters,
			std::string& currentPlotterName
		);
		sfg::Widget::Ptr getWidget() const;

		void Update();
	private:
		std::map<std::string, std::map<std::string, std::unique_ptr<Parameter>>>& m_Parameters;
		std::string& m_CurrentPlotterName;

		sfg::Box::Ptr m_MainBox;
		sfg::ComboBox::Ptr m_ParameterSelector;
		sfg::Box::Ptr m_ParameterBox;
	};
	ParameterPanel m_ParameterPanel;

	class NewDifferentialWindow {
	public:
		NewDifferentialWindow (
			sfg::Desktop& desktop,
			std::function<void(FunctionInformation)> callback
		);
		void Show ( std::set<std::string> functionNames );
	private:
		void Hide();

		sfg::Desktop& m_Desktop;
		std::function<void(FunctionInformation)> m_Callback;

		bool m_HasLowerBound;
		bool m_HasUpperBound;
		bool m_HasMinimumOut;
		bool m_HasMaximumOut;

		sfg::Window::Ptr m_Window;
		sfg::ComboBox::Ptr m_NameSelector;
		sfg::ComboBox::Ptr m_TypeSelector;
		sfg::ComboBox::Ptr m_OrientationSelector;
		sfg::Button::Ptr m_LowerBoundToggle;
		sfg::Button::Ptr m_UpperBoundToggle;
		sfg::Entry::Ptr m_LowerBoundEntry;
		sfg::Entry::Ptr m_UpperBoundEntry;
		sfg::Button::Ptr m_MinimumOutToggle;
		sfg::Button::Ptr m_MaximumOutToggle;
		sfg::Entry::Ptr m_MinimumEntry;
		sfg::Entry::Ptr m_MaximumEntry;
		sfg::Button::Ptr m_PointGapToggle;
		sfg::Entry::Ptr m_PointGapEntry;
		sfg::ComboBox::Ptr m_ColourSelector;
		sfg::Entry::Ptr m_RedEntry;
		sfg::Entry::Ptr m_GreenEntry;
		sfg::Entry::Ptr m_BlueEntry;
		sfg::Button::Ptr m_CancelButton;
		sfg::Button::Ptr m_DoneButton;
	};
	NewDifferentialWindow m_NewDifferentialWindow;
};

}

#endif // PLOTTERPANEL_HPP_INCLUDED
