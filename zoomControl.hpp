#ifndef ZOOMCONTROL_HPP_INCLUDED
#define ZOOMCONTROL_HPP_INCLUDED

//STANDARD LIBRARY INCLUDES
#include <initializer_list> // std::initializer_list
#include <queue>            // std::queue
#include <string>           // std::string
#include <vector>           // std::vector

//SFML SYSTEM INCLUDES
#include <SFML/System/Vector2.hpp> //sf::Vector2f

//SFGUI INCLUDES
#include <SFGUI/Box.hpp>       // sfg::Box
#include <SFGUI/Button.hpp>    // sfg::Button
#include <SFGUI/Desktop.hpp>   // sfg::Desktop
#include <SFGUI/Entry.hpp>     // sfg:Entry
#include <SFGUI/Frame.hpp>     // sfg::Frame
#include <SFGUI/Label.hpp>     // sfg::Label
#include <SFGUI/Scale.hpp>     // sfg::Scale
#include <SFGUI/Separator.hpp> // sfg::Separator
#include <SFGUI/Widget.hpp>    // sfg::Widget
#include <SFGUI/Window.hpp>    // sfg::Window

//INTERNAL INCLUDES
#include "delayedFunction.hpp" // ui::DelayedEvent, ui::DelayedEventQueue

namespace ui {

////////////////////////////////////////////////////////////////////////////////
/// \brief Manages the users control of the zoom
///
/// The zoom is a vector that holds the pixels per unit of graph in each
/// direction. This control contains two text boxes that can be used to alter
/// each axis.
////////////////////////////////////////////////////////////////////////////////
class ZoomControl {

public:

	////////////////////////////////////////////////////////////////////////////
	/// \brief Constructor
	///
	/// Initialises the zoom panel with its starting values.
	///
	/// \param onZoomChange A callback function that is called whenever the zoom
	/// is changed.
	/// \param delayedEventQueue Passed so that the DelayedEvent can be added to
	/// the queue.
	////////////////////////////////////////////////////////////////////////////
	ZoomControl (
		std::function<void(sf::Vector2f, sf::Vector2f, sf::Vector2f)> onZoomChange,
		DelayedEventQueue& delayedEventQueue
	);

	////////////////////////////////////////////////////////////////////////////
	/// \brief Get the widget for the zoom panel.
	///
	/// Returns a pointer to the widget for the basic panel that should go on
	/// the bottom right.
	////////////////////////////////////////////////////////////////////////////
	sfg::Widget::Ptr getWidget() const;

	////////////////////////////////////////////////////////////////////////////
	/// \brief Reset the zoom control.
	///
	/// This should be used when a new file is created. It centres the zoom
	/// control and sets it to 100 pixels per unit in each direction.
	////////////////////////////////////////////////////////////////////////////
	void reset();

	////////////////////////////////////////////////////////////////////////////
	/// \brief Saves the zoom control's state into a queue of strings.
	///
	/// The information put into the queue allows the zoom control to be
	/// restored to its current state at some point in the future.
	////////////////////////////////////////////////////////////////////////////
	void save ( std::queue<std::string>& saveInformation );

	////////////////////////////////////////////////////////////////////////////
	/// \brief Loads the zoom control's state from a queue of strings/
	///
	/// This should be used in tandem with save, as the zoom control expects an
	/// exact format and does not have verification. This might seem like an
	/// issue, however as long as the saving is done by the save function, it
	/// will work as expected.
	////////////////////////////////////////////////////////////////////////////
	void load ( std::queue<std::string>& saveInformation );

private:
	sfg::Frame::Ptr m_Frame;

	DelayedEvent<sf::Vector2f, sf::Vector2f, sf::Vector2f> m_OnZoomChange;

	sfg::Entry::Ptr m_XZoomEntry;
	sfg::Entry::Ptr m_YZoomEntry;
	sfg::Entry::Ptr m_XPosEntry;
	sfg::Entry::Ptr m_YPosEntry;
	sfg::Entry::Ptr m_XSpacingEntry;
	sfg::Entry::Ptr m_YSpacingEntry;
	sfg::Button::Ptr m_SetButton;

	struct Save {
		std::string xZoom;
		std::string yZoom;
		std::string xPos;
		std::string yPos;
		std::string xSpacing;
		std::string ySpacing;
	};
	Save m_Save; //This is used so that the zoom control can be recreated exactly. It avoids having to predict accuracy of numbers.
};

} //END NAMESPACE ui

#endif // ZOOMCONTROL_HPP_INCLUDED
