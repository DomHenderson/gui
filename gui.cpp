//STANDARD LIBRARY INCLUDES
#include <fstream>    //std::fstream
#include <functional> //std::function
#include <queue>      //std::queue
#include <string>     //std::string
#include <utility>    //std::move
#include <vector>     //std::vector

//SFML SYSTEM INCLUDES
#include <SFML/System/Clock.hpp>   //sf::Clock
#include <SFML/System/Vector2.hpp> //sf::Vector2f

//SFML WINDOW INCLUDES
#include <SFML/Window/Event.hpp>     //sf::Event
#include <SFML/Window/VideoMode.hpp> //sf::VideoMode

//SFML GRAPHICS INCLUDES
#include <SFML/Graphics/Color.hpp>        //sf::Color
#include <SFML/Graphics/RenderWindow.hpp> //sf::RenderWindow

//SFGUI INCLUDES
#include <SFGUI/Box.hpp>         //sfg::Box
#include <SFGUI/Button.hpp>      //sfg::Button
#include <SFGUI/CheckButton.hpp> //sfg::CheckButton
#include <SFGUI/ComboBox.hpp>    //sfg::ComboBox
#include <SFGUI/Desktop.hpp>     //sfg::Desktop
#include <SFGUI/Entry.hpp>       //sfg::Entry
#include <SFGUI/Frame.hpp>       //sfg::Frame
#include <SFGUI/Label.hpp>       //sfg::Label
#include <SFGUI/Separator.hpp>   //sfg::Separator
#include <SFGUI/SFGUI.hpp>       //sfg::SFGUI
#include <SFGUI/Window.hpp>      //sfg::Window

//EXPRESSION COMPILER 2 INCLUDES
#include <ExpressionCompiler2/compiler.hpp> //ec::Compiler

//GRAPH CANVAS INCLUDES
#include <GraphCanvas/cartesianAxes.hpp> //gc::CartesianAxes
#include <GraphCanvas/graphCanvas.hpp>   //gc::GraphCanvas
#include <GraphCanvas/plotter.hpp>       //gc::Plotter

//INTERNAL INCLUDES
#include "delayedFunction.hpp"  //ui::DelayedEvent, ui::DelayedEventQueue
#include "gui.hpp"              //ui::Gui
#include "libraryWidget.hpp"    //ui::LibraryWidget
#include "parameter.hpp"        //ui::Parameter
#include "plotterPanel.hpp"     //ui::PlotterPanel
#include "zoomControl.hpp"      //ui::ZoomControl

namespace ui {

//The top panel contains the saving and loading buttons, the DRG selector, and the colour scheme selector.
sfg::Frame::Ptr Gui::CreateTopPanel()
{
	auto newButton = sfg::Button::Create ( "New" );
	auto openButton = sfg::Button::Create ( "Open" );
	auto saveButton = sfg::Button::Create ( "Save" );

	//The window that is shown for the user to choose which file to open.
	//A simple text entry for a filename is used partially because it is simple and the user should not need anything more, and
	//partially to keep the program cross platform.
	auto openWindow = sfg::Window::Create();
	//Open window
	{
		auto box = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
		box->Pack ( sfg::Label::Create ( "Open" ), false, false );
		box->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );

		//Line up the file name entry and its label
		auto midBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
		midBox->Pack ( sfg::Label::Create ( "File name" ), false, false );
		auto fileNameEntry = sfg::Entry::Create ();
		fileNameEntry->SetRequisition ( { 200.f, 0.f } ); //This ensures that the text entry is always a reasonable size.
		midBox->Pack ( fileNameEntry, true, true );
		box->Pack ( midBox, false, true );
		box->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );

		auto cancelButton = sfg::Button::Create ( "Cancel" );
		auto doneButton = sfg::Button::Create ( "Done" );
		auto buttonBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
		buttonBox->Pack ( cancelButton, true, true );
		buttonBox->Pack ( doneButton, true, true );
		box->Pack ( buttonBox, false, true );

		//Simply hide the window if the cancel button is pressed
		cancelButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ openWindow ] () {
			openWindow->Show ( false );
		} );

		//This function and the saveWindow's doneButton's signal must be kept in sync, otherwise they will not work correctly.
		doneButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this, openWindow, fileNameEntry ] () {
			std::fstream fs;
			std::string fileName = fileNameEntry->GetText() + ".txt";
			fs.open ( fileName, std::fstream::in );

			//Storing the information from the file in a queue of strings allows each class to simply take the lines it needs.
			std::string line ("");
			std::queue<std::string> saveInformation;
			while ( std::getline ( fs, line ) ) {
				saveInformation.push ( line );
			}
			fs.close();

			//Set everything up using the information from the save file.
			m_UpdateQueue.clear();
			m_DelayedEventQueue.clear();
			m_ZoomControl.load ( saveInformation );
			m_Library.load ( saveInformation );
			m_PlotterPanel.load ( saveInformation );
			m_DRGSelector->SelectItem ( std::stoi ( saveInformation.front() ) );
			saveInformation.pop();
			m_DRGSelector->GetSignal ( sfg::ComboBox::OnSelect )();

			//Hide the open window now that its purpose is done.
			openWindow->Show ( false );
		} );

		openWindow->Add ( box );
		m_Desktop.Add ( openWindow );
		openWindow->Show ( false );
	}

	//This window is almost exactly the same as the openWindow, however they are so simple that it's not worth creating a class for them.
	auto saveWindow = sfg::Window::Create();
	//Save window
	{
		auto box = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
		box->Pack ( sfg::Label::Create ( "Save" ), false, false );
		box->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );

		auto midBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
		midBox->Pack ( sfg::Label::Create ( "File name" ), false, false );
		auto fileNameEntry = sfg::Entry::Create ();
		fileNameEntry->SetRequisition ( { 200.f, 0.f } );
		midBox->Pack ( fileNameEntry, true, true );
		box->Pack ( midBox, false, true );
		box->Pack ( sfg::Separator::Create ( sfg::Separator::Orientation::HORIZONTAL ), false, true );

		auto cancelButton = sfg::Button::Create ( "Cancel" );
		auto doneButton = sfg::Button::Create ( "Done" );
		auto buttonBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
		buttonBox->Pack ( cancelButton, true, true );
		buttonBox->Pack ( doneButton, true, true );
		box->Pack ( buttonBox, false, true );

		cancelButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ saveWindow ] () {
			saveWindow->Show ( false );
		} );

		//Just as the openWindow reads the file into a queue and then passes it around, this passes an empty queue around for each
		//class to fill with its save information, and then stores that in a file.
		doneButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this, saveWindow, fileNameEntry ] () {
			std::queue<std::string> saveInformation;
			m_ZoomControl.save ( saveInformation );
			m_Library.save ( saveInformation );
			m_PlotterPanel.save ( saveInformation );
			saveInformation.push ( std::to_string ( m_DRGSelector->GetSelectedItem() ) );

			std::fstream fs;
			std::string fileName = fileNameEntry->GetText() + ".txt";
			fs.open ( fileName, std::fstream::out | std::fstream::trunc ); //trunc is used to overwrite any existing file contents.
			while ( !saveInformation.empty() ) {
				fs<<saveInformation.front()<<std::endl;
				saveInformation.pop();
			}
			fs.close();

			saveWindow->Show ( false );
		} );

		saveWindow->Add ( box );
		m_Desktop.Add ( saveWindow );
		saveWindow->Show ( false );
	}

	newButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this ] () {
		m_UpdateQueue.clear();
		m_DelayedEventQueue.clear();
		m_ZoomControl.reset();
		m_Library.reset();
		m_PlotterPanel.reset();
	} );

	openButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this, openWindow ] () {
		openWindow->Show ( true );
		m_Desktop.BringToFront ( openWindow );
	} );

	saveButton->GetSignal ( sfg::Button::OnLeftClick ).Connect ( [ this, saveWindow ] () {
		saveWindow->Show ( true );
		m_Desktop.BringToFront ( saveWindow );
	} );

	auto fileBox = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	fileBox->Pack ( newButton, false, false );
	fileBox->Pack ( openButton, false, false );
	fileBox->Pack ( saveButton, false, false );

	auto DRGLabel = sfg::Label::Create ( "Angles" );
	m_DRGSelector = sfg::ComboBox::Create ();
	m_DRGSelector->AppendItem ( "Radians" );
	m_DRGSelector->AppendItem ( "Degrees" );
	m_DRGSelector->AppendItem ( "Gradians" );
	m_DRGSelector->SelectItem ( 0 );

	m_DRGSelector->GetSignal ( sfg::ComboBox::OnSelect ).Connect (
		[ this ] {
			switch ( m_DRGSelector->GetSelectedItem() ) {
			case 0: //Radians
				m_Compiler.setTrigMode ( ec::Compiler::DRG::Radians );
				break;

			case 1: //Degrees
				m_Compiler.setTrigMode ( ec::Compiler::DRG::Degrees );
				break;

			case 2: //Gradians
				m_Compiler.setTrigMode ( ec::Compiler::DRG::Gradians );
				break;
			}
		}
	);

	auto DRGBox = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
	DRGBox->Pack ( DRGLabel, false, false );
	DRGBox->Pack ( m_DRGSelector, false, true );

	auto DRGFrame = sfg::Frame::Create ();
	DRGFrame->Add ( DRGBox );
	DRGFrame->SetAlignment ( { 1.f, 0.f } );

	auto schemeLabel = sfg::Label::Create ( "Colour scheme" );
	m_ColourSchemeSelection = sfg::ComboBox::Create ( );
	m_ColourSchemeSelection->AppendItem ( "Tron" );
	m_ColourSchemeSelection->AppendItem ( "Tron (alt)" );
	m_ColourSchemeSelection->AppendItem ( "Light" );
	m_ColourSchemeSelection->AppendItem ( "Night" );
	m_ColourSchemeSelection->SelectItem ( 0 );

	m_ColourSchemeSelection->GetSignal ( sfg::ComboBox::OnSelect ).Connect (
		[ this ] {
			//Switch colour scheme refers to m_ColourSchemeSelection directly, so there's no need to pass anything.
			SwitchColourScheme();
		}
	);

	auto schemeBox = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 5.f );
	schemeBox->Pack ( schemeLabel, false, false );
	schemeBox->Pack ( m_ColourSchemeSelection, false, false );

	auto schemeFrame = sfg::Frame::Create();
	schemeFrame->Add ( schemeBox );
	schemeFrame->SetAlignment ( { 1.f, 0.f } );

	auto box = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	box->Pack ( fileBox, true, true );
	box->Pack ( DRGFrame, false, false );
	box->Pack ( schemeFrame, false, false );

	auto frame = sfg::Frame::Create();
	frame->Add ( box );
	frame->SetAlignment ( { 0.5f, 0.f } );
	return frame;
}

sfg::Frame::Ptr Gui::CreateLeftPanel()
{
	//The contents of the left panel are encapsulated in the LibraryWidget class
	auto frame = sfg::Frame::Create();
	frame->Add ( m_Library.getWidget() );
	frame->SetAlignment ( {0.f, 0.5f} );

	return frame;
}

sfg::Frame::Ptr Gui::CreateMidPanel()
{
	//Although the frame does not actually contain widgets, it is used for aesthetic reasons,
	//and to make sure that the gap in the centre is maintained.
	auto frame = sfg::Frame::Create();

	//The default window size is 800px x 800px, and the default zoom is 100pixels per graph unit, so the axes go from -4 to 4.
	gc::CartesianAxes cartesianAxes ( -4, 4, -4, 4, 1, 1 );
	cartesianAxes.setAxesColour ( { 0, 255, 255 } );
	m_GraphCanvas.addCartesianAxes ( "mainAxes", cartesianAxes );

	return frame;
}

sfg::Frame::Ptr Gui::CreateRightPanel()
{
	//The contents of the right panel are encapsulated away in the ParameterPanel.
	//ParameterPanel is part of PlotterPanel so that the PlotterPanel can control the ParameterPanel more easily.
	auto frame = sfg::Frame::Create();
	frame->Add ( m_PlotterPanel.getParameterPanelWidget() );
	return frame;
}

sfg::Frame::Ptr Gui::CreateBottomPanel()
{
	//The bottom panel is split into two sections.
	//The plotter panel handles the functions, parameters, plotters etc.
	//The zoomControl goes in the bottom right corner and controls the camera.
	auto box = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	box->Pack ( m_PlotterPanel.getWidget(), true, true );
	box->Pack ( m_ZoomControl.getWidget(), false, true );

	auto frame = sfg::Frame::Create();
	frame->Add ( box );
	return frame;
}

//One of the preset colour schemes is applied
void Gui::SetThemePreferences ( ColourScheme colourScheme )
{
	sf::Color mainBorder;
	sf::Color text;
	sf::Color invalidText;
	sf::Color contrastBorder;
	sf::Color background;
	sf::Color button;

	switch ( colourScheme ) {
	case ColourScheme::Tron:
		mainBorder = { 223, 116, 12 }; //Orange
		text = { 255, 230, 77 }; //Yellow
		contrastBorder = { 111, 195, 223 }; //Cyan blue
		background = { 12, 20, 31 }; //Dark grey
		button = { 85, 87, 82 }; //Medium grey
		invalidText = { 255, 0, 0 }; //Red
		m_BackgroundColour = {0,0,0}; //Black
		break;

	case ColourScheme::TronAlt:
		mainBorder = { 111, 195, 223 }; //Cyan blue
		text = { 192, 255, 255 }; //Very light blue
		contrastBorder = { 223, 116, 12 }; //Orange
		background = { 12, 20, 31 }; //Dark grey
		button = { 85, 87, 82 }; //Medium grey
		invalidText = { 255, 0, 0 }; //Red
		m_BackgroundColour = {0,0,0}; //Black
		break;

	case ColourScheme::Light:
		mainBorder = { 128, 128, 128 }; //Light grey
		text = { 0, 0, 0 }; //Black
		contrastBorder = { 0, 0, 255 }; //Blue
		background = { 255, 255, 255 }; //White
		button = { 196, 196, 196 }; //Grey
		invalidText = { 255, 0, 0 }; //Red
		m_BackgroundColour = {255,255,255}; //White
		break;

	case ColourScheme::Night:
		mainBorder = { 64, 64, 64 }; //Dark grey
		text = { 112, 160, 160 }; //Light grey/blue
		contrastBorder = { 128, 128, 128 }; //Light grey
		background = { 0, 0, 0 }; //Black
		button = { 85, 87, 82 }; //Medium grey
		invalidText = { 255, 0, 0 }; //Red
		m_BackgroundColour = {0,0,0}; //Black
		break;
	}

	m_Desktop.SetProperty ( "Button", "BorderWidth", 2.f );
	m_Desktop.SetProperty ( "Button", "Color", text );
	m_Desktop.SetProperty ( "Button", "BackgroundColor", button );

	m_Desktop.SetProperty ( "ComboBox", "BorderWidth", 2.f );
	m_Desktop.SetProperty ( "ComboBox", "Color", text );
	m_Desktop.SetProperty ( "ComboBox", "BackgroundColor", button );
	m_Desktop.SetProperty ( "ComboBox", "HighlightedColor", contrastBorder );

	m_Desktop.SetProperty ( "Frame", "BorderColor", mainBorder );
	m_Desktop.SetProperty ( "Frame", "BorderWidth", 2.f );

	m_Desktop.SetProperty ( "Window", "BackgroundColor", background );
	m_Desktop.SetProperty ( "Window", "BorderColor", text );
	m_Desktop.SetProperty ( "Window", "BorderWidth", 2.f );
	m_Desktop.SetProperty ( "Window", "TitleBackgroundColor", mainBorder );
	m_Desktop.SetProperty ( "Window", "Color", text );

	m_Desktop.SetProperty ( "Entry", "BorderColor", contrastBorder );
	m_Desktop.SetProperty ( "Entry", "BorderWidth", 2.f );
	m_Desktop.SetProperty ( "Entry", "Color", text );
	m_Desktop.SetProperty ( "Entry", "BackgroundColor", background );
	m_Desktop.SetProperty ( "Entry#Invalid", "Color", invalidText );

	m_Desktop.SetProperty ( "Scale", "BorderWidth", 2.f );
	m_Desktop.SetProperty ( "Scale", "TroughColor", contrastBorder );
	m_Desktop.SetProperty ( "Scale", "SliderColor", button );

	m_Desktop.SetProperty ( "ScrolledWindow", "BorderColor", contrastBorder );
	m_Desktop.SetProperty ( "ScrolledWindow", "BorderColorShift", -32 );
	m_Desktop.SetProperty ( "ScrolledWindow", "BorderWidth", 2.f );

	m_Desktop.SetProperty ( "Label", "Color", text );

	m_Desktop.SetProperty ( "SpinButton", "Color", text );
	m_Desktop.SetProperty ( "SpinButton", "StepperArrowColor", text );
	m_Desktop.SetProperty ( "SpinButton", "BackgroundColor", background );
	m_Desktop.SetProperty ( "SpinButton", "StepperBackgroundColor", button );
	m_Desktop.SetProperty ( "SpinButton#Invalid", "Color", text );

	m_Desktop.SetProperty ( "ProgressBar", "BarColor", contrastBorder );
	m_Desktop.SetProperty ( "ProgressBar", "BarBorderColor", contrastBorder );

	m_Desktop.SetProperty ( "Notebook", "BackgroundColor", sf::Color ( 0,0,0,0 ) );
	m_Desktop.SetProperty ( "Notebook", "BackgroundColorDark", sf::Color ( 0,0,0,0 ) );
	m_Desktop.SetProperty ( "Notebook", "BackgroundColorPrelight", background );
	m_Desktop.SetProperty ( "Notebook", "BorderColor", sf::Color ( 0,0,0,0 ) );
	m_Desktop.SetProperty ( "Notebook", "BorderWidth", 2.f );
	m_Desktop.SetProperty ( "Notebook", "Padding", 1.f );
}

Gui::Gui() :
	m_Compiler ( [] ( std::string message ) { //The compiler is set up with a function to relay errors to the user.
		std::cout<<message<<std::endl;
	} ),
	m_Library ( m_DelayedEventQueue, m_Desktop, m_Compiler ),
	m_ZoomControl (
		[ this ] ( sf::Vector2f pixelsPerUnit, sf::Vector2f centre, sf::Vector2f spacing ) {
			m_PixelsPerUnit = pixelsPerUnit;
			float width = m_RenderWindow.getSize().x;
			float height = m_RenderWindow.getSize().y;
			m_GraphCanvas.getCartesianAxes ( "mainAxes" ).setLowerXBound ( centre.x - width / ( m_PixelsPerUnit.x ) );
			m_GraphCanvas.getCartesianAxes ( "mainAxes" ).setUpperXBound ( centre.x + width / ( m_PixelsPerUnit.x ) );
			m_GraphCanvas.getCartesianAxes ( "mainAxes" ).setLowerYBound ( centre.y - height / ( m_PixelsPerUnit.y ) );
			m_GraphCanvas.getCartesianAxes ( "mainAxes" ).setUpperYBound ( centre.y + height / ( m_PixelsPerUnit.y ) );
			m_GraphCanvas.getCartesianAxes ( "mainAxes" ).setXMarkSpacing ( spacing.x );
			m_GraphCanvas.getCartesianAxes ( "mainAxes" ).setYMarkSpacing ( spacing.y );
			m_GraphCanvas.setGraphArea ( { centre.x -width / ( 2 * m_PixelsPerUnit.x ), centre.y + height / ( 2 * m_PixelsPerUnit.y ), width / m_PixelsPerUnit.x, -height / m_PixelsPerUnit.y } );
			m_GraphCanvas.setDrawArea ( { 0,0, width, height } );
			m_RenderWindow.setView ( sf::View ( { width/2, height/2 }, { width, height } ) );
		},
		m_DelayedEventQueue
	),
	m_RenderWindow ( sf::VideoMode ( 800, 800 ), "Graph drawer" ),
	SwitchColourScheme ( m_DelayedEventQueue, [ this ] {
		switch ( m_ColourSchemeSelection->GetSelectedItem() ) {
		case 0:
			this->SetThemePreferences ( ColourScheme::Tron );
			break;

		case 1:
			this->SetThemePreferences ( ColourScheme::TronAlt );
			break;

		case 2:
			this->SetThemePreferences ( ColourScheme::Light );
			break;

		case 3:
			this->SetThemePreferences ( ColourScheme::Night );
			break;
		}

	} ),
	m_GraphCanvas ( {0,0,800,800}, {-4,4,8,-8} ),
	m_PixelsPerUnit ( 100.f, 100.f ),
	m_PlotterPanel ( m_Desktop, m_GraphCanvas, m_Compiler, m_UpdateQueue, m_DelayedEventQueue ),
	m_BackgroundColour ( { 0,0,0 } )
{
	//NO_STYLE means that the window does not appear to be a window, but is simply a transparent container.
	m_MainWindow = sfg::Window::Create ( sfg::Window::Style::NO_STYLE );
	//Padding is already done by midRow, so this one doesn't need it.
	auto box = sfg::Box::Create ( sfg::Box::Orientation::VERTICAL, 0.f );
	auto midRow = sfg::Box::Create ( sfg::Box::Orientation::HORIZONTAL, 5.f );
	box->Pack ( CreateTopPanel(), false, true ); //The Create*Panel functions are used to make this part a lot more readable.
	midRow->Pack ( CreateLeftPanel(), false, true );
	midRow->Pack ( CreateMidPanel(), true, true );
	midRow->Pack ( CreateRightPanel(), false, true );
	box->Pack ( midRow, true, true );
	box->Pack ( CreateBottomPanel(), false, true );

	m_MainWindow->Add ( box );

	m_MainWindow->SetAllocation ( { //The main SFGUI window must take up the entire SFML window.
		0,
		0,
		static_cast<float> ( m_RenderWindow.getSize().x ),
		static_cast<float> ( m_RenderWindow.getSize().y )
	} );
	m_MainWindow->SetRequisition ( {//This ensures it does not get resized by SFGUI.
		static_cast<float> ( m_RenderWindow.getSize().x ),
		static_cast<float> ( m_RenderWindow.getSize().y )
	} );

	m_Desktop.Add ( m_MainWindow );
	SetThemePreferences ( ColourScheme::Tron );

	m_Compiler.setTrigMode ( ec::Compiler::DRG::Radians ); //This is to prevent reliance on the Compiler internally having radians as its default.
}

void Gui::processEvents()
{
	sf::Event event;

	while ( m_RenderWindow.pollEvent ( event ) ) {
		m_Desktop.HandleEvent ( event );

		if ( event.type == sf::Event::Resized ) {
			//Must be done in this order because otherwise it breaks after exiting from fullscreen.
			m_MainWindow->SetRequisition ( {
				static_cast<float> ( event.size.width ),
				static_cast<float> ( event.size.height )
			} );
			m_MainWindow->SetAllocation ( {
				0,
				0,
				static_cast<float> ( event.size.width ),
				static_cast<float> ( event.size.height )
			} );
			float width = event.size.width;
			float height = event.size.height;
			m_GraphCanvas.getCartesianAxes ("mainAxes").setLowerXBound ( -width/(m_PixelsPerUnit.x) );
			m_GraphCanvas.getCartesianAxes ("mainAxes").setUpperXBound ( width/(m_PixelsPerUnit.x) );
			m_GraphCanvas.getCartesianAxes ("mainAxes").setLowerYBound ( -height/(m_PixelsPerUnit.y) );
			m_GraphCanvas.getCartesianAxes ("mainAxes").setUpperYBound ( height/(m_PixelsPerUnit.y) );
			m_GraphCanvas.setGraphArea ( { -width/(2*m_PixelsPerUnit.x), height/(2*m_PixelsPerUnit.y), width/m_PixelsPerUnit.x, -height/m_PixelsPerUnit.y } );
			m_GraphCanvas.setDrawArea ( { 0,0, width, height } );
			m_RenderWindow.setView ( sf::View ( { width/2, height/2 }, { width, height } ) );
		}

		// If window is about to be closed, leave program.
		if ( event.type == sf::Event::Closed ) {
			m_RenderWindow.close();
		}
	}

	//Run any DelayedEvents which have had their operator() called
	m_DelayedEventQueue.run();

	//This causes any Widgets which were affected by the delayed events to be
	//properly resized
	m_Desktop.Refresh();
}

void Gui::update()
{
	//Some functions are based on the time since the last frame.
	//sf::Clock:restart simulatenously returns the time stored in the clock and resets it.
	float time = m_Clock.restart().asSeconds();
	m_Desktop.Update ( time );

	for( unsigned i (0); i < m_UpdateQueue.size(); ++i ) {
		if ( auto ptr = m_UpdateQueue[i].lock() ) {
			(*ptr)(time);
		} else {
			m_UpdateQueue.erase ( m_UpdateQueue.begin() + i-- );
		}
	}

	//Update the positions of all the axes' and functions' vertices.
	m_GraphCanvas.update();
}

void Gui::draw()
{
	m_RenderWindow.clear( m_BackgroundColour );
	m_GraphCanvas.preDraw(); //Apply the Fx.
	m_RenderWindow.draw ( m_GraphCanvas ); //Draw the graph
	m_Sfgui.Display ( m_RenderWindow ); //Draw the gui on top of the graph
	m_RenderWindow.display(); //Display the result to the screen
}

bool Gui::isOpen()
{
	return m_RenderWindow.isOpen();
}

} //END NAMESPACE ui
